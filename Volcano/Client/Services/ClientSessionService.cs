﻿using System;

namespace Volcano.Client.Services
{
    public class ClientSessionService
    {
        public event EventHandler<bool> SessionStateChanged;

        public Guid UserId { get; set; }
        public string UserName { get; set; }

        bool _IsLoggedIn=false;
        public bool IsLoggedIn { get { return _IsLoggedIn; } set { _IsLoggedIn = value; SessionStateChanged?.Invoke(this, value); } } 
        public Guid SecToken { get; set; }
        public bool IsAdmin { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool IsActive { get; set; }
    }
}