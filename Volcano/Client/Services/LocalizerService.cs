﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Volcano.Client.Services
{
    public class LocalizerService
    {
        public event EventHandler<Language> CurrantLanguageChanged;

        Dictionary<string, (string, string)> translation;


        Language _CurrantLanguage;
        public Language CurrantLanguage
        {
            get { return _CurrantLanguage; }
            set
            {
                _CurrantLanguage = value;
                CurrantLanguageChanged?.Invoke(this, _CurrantLanguage);
            }
        }

        public string OtherLanguage { get => _CurrantLanguage == Language.Arabic ? "English" : "عربى"; }
        public string Direction { get => _CurrantLanguage == Language.Arabic ? "RTL" : "LTR"; }
        public LocalizerService()
        {
            translation = new()
            {
                { nameof(Home), ("Home", "الرئيسية") },
                { nameof(Customers), ("Customers", "العملاء") },
                { nameof(Representatives), ("Representatives", "المندوبين") },
                { nameof(Supervisors), ("Supervisors", "المشرفين") },
                { nameof(Users), ("Users", "المستخدمين") },
                { nameof(JobOrders), ("Job Orders", "اوامر الشغل") },
                { nameof(Reports), ("Reports", "التقارير") },

                { nameof(Hello), ("Hello", "مرحباً") },
                { nameof(Register), ("Register", "تسجيل") },
                { nameof(LogIn), ("Log in", "دخول") },
                { nameof(LogOut), ("Log out", "خروج") },
                { nameof(InvalidUsernameOrPassword), ("Invalid Username or Password", "اسم المستخدم أو كلمة المرور غير صحيحة") },

                { nameof(NoContent), ("Sorry, there's nothing at this address.", "عفواً, لايوجد شئ هنا ليتم عرضه") },
                { nameof(NotAuthorized), ("You are not authorized to access this resource.", "عفواً, لايمكنك عرض هذا المحتوى.") },

                { nameof(Name), ("Name", "الاسم") },
                { nameof(NameEn), ("Name (in latin)", " (باللاتينى)الاسم") },
                { nameof(Save), ("Save", "حفظ") },
                { nameof(Cancel), ("Cancel", "الغاء") },
                { nameof(Phone), ("Phone", "الهاتف") },
                { nameof(Active), ("Active", "نشط") },
                { nameof(Edit), ("Edit", "تعديل") },
                { nameof(Delete), ("Delete", "حزف") },
                { nameof(Yes), ("Yes", "نعم") },
                { nameof(No), ("No", "لا") },
                { nameof(Confirm), ("Confirm", "تاكيد") },
                { nameof(ConfirmMessage), ("Are you sure you want to delete the selectd item?", "هل انت متاكد انك تريد حزف هذا العنصر؟") },
  { nameof(CannotDelete), ("It cannot be deleted because there is data associated with this statement", "لا يمكن الحذف لوجود بيانات مرتبطه بهذا البيان") },
                { nameof(NewSupervisor), ("New Supervisor", "أضافة مشرف") },
                { nameof(EditSupervisor), ("Edit Supervisor", "تعديل مشرف") },
                { nameof(NewRepresentative), ("New Representative", "أضافة مندوب") },
                { nameof(EditRepresentative), ("Edit Representative", "تعديل مندوب") },
                { nameof(Nationality), ("Nationality", "الجنسية") },
                { nameof(Address), ("Address", "العنوان") },
                { nameof(Report), ("Report", "تقرير") },
                { nameof(BirthDate), ("BirthDate", "تاريخ الميلاد") },
                { nameof(StartingDate), ("Starting Date", "تاريخ بداية العمل") },
                { nameof(ResidencyDate), ("Residency Date", "تاريخ الاقامة") },
                { nameof(ResidencyEndDate), ("Residency End Date", "تاريخ انتهاء الاقامة") },
                { nameof(ResidencyNumber), ("Residency Number", "رقم الاقامة") },
                { nameof(LicenseNumber), ("License Number", "رقم الرخصة") },
                { nameof(PassportDate), ("Passport Date", "تاريخ الجواز") },
                { nameof(PassportEndDate), ("Passport End Date", "تاريخ انتهاء الجواز") },
                { nameof(PassportNumber), ("Passport Number", "رقم الجواز") },
                { nameof(LicenseDate), ("License Date", "تاريخ الرخصة") },
                { nameof(LicenseEndDate), ("License End Date", "تاريخ انتهاء الرخصة") },
                { nameof(SupervisorAdded), ("Supervisor Added", "تم حفظ بيانات المشرف") },
                { nameof(NoRows), ("No records to display.", "لايوجد بيانات لعرضها.") },
                { nameof(Devices), ("Devices", "الاجهزة") },
                { nameof(Show), ("Show", "عرض") },
                { nameof(UserNameAlreadyExists), ("UserName Already Exists", "أسم المستخدم موجود بالفعل") },
                { nameof(Services), ("Services", "الخدمات") },
                { nameof(Notes), ("Notes", "ملاحظات") },
                { nameof(Password), ("Password", "كلمة المرور") },
                { nameof(Data), ("Data", "البيانات") },
                { nameof(Customer), ("Customer", "عميل") },
                { nameof(NewDevice), ("New Device", "أضافة جهاز") },
                { nameof(NewService), ("New Service", "أضافة خدمة") },
                { nameof(NewCustomer), ("New Customer", "أضافة عميل") },
                { nameof(EditDevice), ("Edit Device", "تعديل جهاز") },
                { nameof(EditService), ("Edit Service", "تعديل خدمة") },
                { nameof(EditCustomer), ("Edit Customer", "تعديل عميل") },
                { nameof(DeviceSaved), ("Device Saved", "تم حفظ بيانات الجهاز") },
                { nameof(ServiceSaved), ("Service Saved", "تم حفظ بيانات الخدمة") },
                { nameof(RepresentativeSaved), ("Representative Saved", "تم حفظ بيانات المندوب") },
                { nameof(Representative), ("Representative", "المندوب") },
                { nameof(NewUser), ("New User", "اضافة مستخدم") },
                { nameof(UserName), ("User Name", "اسم المستخدم") },
                { nameof(Email), ("Email", "البريد الالكترونى") },
                { nameof(Service), ("Service", "الخدمة") },
                { nameof(CustomerReport), ("Customer Report", "تقرير عميل") },
                { nameof(CanEdit), ("Can Edit", "يمكنه التعديل") },
                { nameof(CanDelete), ("Can Delete", "يمكنه الحزف") },
                { nameof(IsAdmin), ("Is Admin", "مدير") },
                { nameof(ResponsibleName), ("Responsible Name", "اسم المسؤل") },
                { nameof(Device), ("Device", "الجهاز") },
                { nameof(IsDone), ("Done", "تم") },
                { nameof(Plans), ("Plans", "الخطط") },
                { nameof(Print), ("Print", "طباعة") },
                { nameof(BatchNumber), ("Batch Number", "رقم التشغيل") },
                { nameof(ApproveSparePartsChange), ("Approve Spare Parts Change", "تعميد بتغيير قطع الغيار") },
                { nameof(MaintenanceAndRepair), ("Maintenance And Repair", "صيانة وتشييك") },
                { nameof(MaintenanceOfExtinguishers), ("Maintenance Of Extinguishers", "صيانة طفايات") },
                { nameof(Statement), ("Statement", "البيان") },
                { nameof(Refresh), ("Refresh", "تحديث") },
                { nameof(Welcome), ("Welcome", "مرحباً") },
                { nameof(Search), ("Search", "بحث") },
                { nameof(SelectSupervisor), ("Select Supervisor", "اختر مشرف") },
                { nameof(PasswordsMustBeIdentical), ("Passwords must be identical", "يجب ان تتطابق كلمتى المرور") },
                { nameof(SelectCustomer), ("Select Customer", "اختر عميل") },
                { nameof(SiteStudy), ("Site Study", "دراسة موقع") },
                { nameof(RepairFailures), ("Repair Failures", "اصلاح اعطال") },
                { nameof(Done), ("Done", "ما تم") },
                { nameof(ReportPreparing), ("Report Preparing", "تجهيز تقرير") },
                { nameof(SpareParts), ("Spare Parts", "قطع الغيار") },
                { nameof(ResponsiblePhone), ("Responsible Phone", "هاتف المسؤل") },
                { nameof(InvoiceNumber), ("Invoice Number", "رقم الفاتورة") },
                { nameof(Supervisor), ("Supervisor", "المشرف") },
                { nameof(SelectRepresentative), ("Select Representative", "اختر مندوب") },
                { nameof(JobOrderReport), ("JobOrder Report", "تقرير أمر شغل") },
                { nameof(JobOrder), ("Job Order", "أمر شغل") },
                { nameof(NotFound), ("Not Found", "غير موجود") },
                { nameof(RepresentativeReport), ("Representative Report", "تقرير مندوب") },
                { nameof(SupervisorReport), ("Supervisor Repor", "تقرير مشرف") },
                { nameof(NewJobOrder), ("New Job Order", "اضافة امر شغل") },
                { nameof(EditJobOrder), ("Edit Job Order", "تعديل امر شغل") },
                { nameof(SelectJobOrder), ("Select Job Order", "اختار امر شغل") },
            };
        }

        public void ChangeLanguage() => CurrantLanguage = CurrantLanguage == Language.English ? Language.Arabic : Language.English;

        string GetValue(string key) => _CurrantLanguage == Language.English ? translation[key].Item1 : translation[key].Item2;

        public string Home { get => GetValue(nameof(Home)); }
        public string Customers { get => GetValue(nameof(Customers)); }
        public string Representatives { get => GetValue(nameof(Representatives)); }
        public string Supervisors { get => GetValue(nameof(Supervisors)); }
        public string Users { get => GetValue(nameof(Users)); }
        public string JobOrders { get => GetValue(nameof(JobOrders)); }
        public string Reports { get => GetValue(nameof(Reports)); }
        public string Hello { get => GetValue(nameof(Hello)); }
        public string Register { get => GetValue(nameof(Register)); }
        public string LogIn { get => GetValue(nameof(LogIn)); }
        public string LogOut { get => GetValue(nameof(LogOut)); }
        public string NoContent { get => GetValue(nameof(NoContent)); }
        public string NotAuthorized { get => GetValue(nameof(NotAuthorized)); }
        public string Name { get => GetValue(nameof(Name)); }
        public string NameEn { get => GetValue(nameof(NameEn)); }
        public string Phone { get => GetValue(nameof(Phone)); }
        public string Active { get => GetValue(nameof(Active)); }
        public string Edit { get => GetValue(nameof(Edit)); }
        public string Delete { get => GetValue(nameof(Delete)); }
        public string Yes { get => GetValue(nameof(Yes)); }
        public string No { get => GetValue(nameof(No)); }
        public string Confirm { get => GetValue(nameof(Confirm)); }
        public string Print { get => GetValue(nameof(Print)); }
        public string Search { get => GetValue(nameof(Search)); }
        public string NotFound { get => GetValue(nameof(NotFound)); }
        public string ConfirmMessage { get => GetValue(nameof(ConfirmMessage)); }
        public string NewSupervisor { get => GetValue(nameof(NewSupervisor)); }
        public string EditSupervisor { get => GetValue(nameof(EditSupervisor)); }
        public string NewRepresentative { get => GetValue(nameof(NewRepresentative)); }
        public string EditRepresentative { get => GetValue(nameof(EditRepresentative)); }
        public string Nationality { get => GetValue(nameof(Nationality)); }
        public string BirthDate { get => GetValue(nameof(BirthDate)); }
        public string StartingDate { get => GetValue(nameof(StartingDate)); }
        public string ResidencyDate { get => GetValue(nameof(ResidencyDate)); }
        public string ResidencyEndDate { get => GetValue(nameof(ResidencyEndDate)); }
        public string ResidencyNumber { get => GetValue(nameof(ResidencyNumber)); }
        public string PassportDate { get => GetValue(nameof(PassportDate)); }
        public string PassportEndDate { get => GetValue(nameof(PassportEndDate)); }
        public string PassportNumber { get => GetValue(nameof(PassportNumber)); }
        public string LicenseDate { get => GetValue(nameof(LicenseDate)); }
        public string LicenseEndDate { get => GetValue(nameof(LicenseEndDate)); }
        public string LicenseNumber { get => GetValue(nameof(LicenseNumber)); }
        public string Address { get => GetValue(nameof(Address)); }
        public string Save { get => GetValue(nameof(Save)); }
        public string Cancel { get => GetValue(nameof(Cancel)); }
        public string SupervisorAdded { get => GetValue(nameof(SupervisorAdded)); }
        //public string SupervisorUpdated { get => GetValue(nameof(SupervisorUpdated)); }
        public string NoRows { get => GetValue(nameof(NoRows)); }
        public string Devices { get => GetValue(nameof(Devices)); }
        public string NewDevice { get => GetValue(nameof(NewDevice)); }
        public string EditDevice { get => GetValue(nameof(EditDevice)); }
        public string DeviceSaved { get => GetValue(nameof(DeviceSaved)); }
        public string RepresentativeSaved { get => GetValue(nameof(RepresentativeSaved)); }
        public string Services { get => GetValue(nameof(Services)); }
        public string NewService { get => GetValue(nameof(NewService)); }
        public string EditService { get => GetValue(nameof(EditService)); }
        public string ServiceSaved { get => GetValue(nameof(ServiceSaved)); }
        public string NewCustomer { get => GetValue(nameof(NewCustomer)); }
        public string CannotDelete { get => GetValue(nameof(CannotDelete)); }
        public string EditCustomer { get => GetValue(nameof(EditCustomer)); }
        public string Representative { get => GetValue(nameof(Representative)); }
        public string NewUser { get => GetValue(nameof(NewUser)); }
        public string UserName { get => GetValue(nameof(UserName)); }
        public string Email { get => GetValue(nameof(Email)); }
        public string CanEdit { get => GetValue(nameof(CanEdit)); }
        public string CanDelete { get => GetValue(nameof(CanDelete)); }
        public string IsAdmin { get => GetValue(nameof(IsAdmin)); }
        public string ResponsibleName { get => GetValue(nameof(ResponsibleName)); }
        public string Notes { get => GetValue(nameof(Notes)); }
        public string Customer { get => GetValue(nameof(Customer)); }
        public string Service { get => GetValue(nameof(Service)); }
        public string Device { get => GetValue(nameof(Device)); }
        public string NewJobOrder { get => GetValue(nameof(NewJobOrder)); }
        public string InvoiceNumber { get => GetValue(nameof(InvoiceNumber)); }
        public string Supervisor { get => GetValue(nameof(Supervisor)); }
        public string IsDone { get => GetValue(nameof(IsDone)); }
        public string ResponsiblePhone { get => GetValue(nameof(ResponsiblePhone)); }
        public string Statement { get => GetValue(nameof(Statement)); }
        public string SpareParts { get => GetValue(nameof(SpareParts)); }
        public string Done { get => GetValue(nameof(Done)); }
        public string ReportPreparing { get => GetValue(nameof(ReportPreparing)); }
        public string MaintenanceOfExtinguishers { get => GetValue(nameof(MaintenanceOfExtinguishers)); }
        public string MaintenanceAndRepair { get => GetValue(nameof(MaintenanceAndRepair)); }
        public string RepairFailures { get => GetValue(nameof(RepairFailures)); }
        public string ApproveSparePartsChange { get => GetValue(nameof(ApproveSparePartsChange)); }
        public string SiteStudy { get => GetValue(nameof(SiteStudy)); }
        public string EditJobOrder { get => GetValue(nameof(EditJobOrder)); }
        public string BatchNumber { get => GetValue(nameof(BatchNumber)); }
        public string Plans { get => GetValue(nameof(Plans)); }
        public string Welcome { get => GetValue(nameof(Welcome)); }
        public string Data { get => GetValue(nameof(Data)); }
        public string Refresh { get => GetValue(nameof(Refresh)); }
        public string SelectCustomer { get => GetValue(nameof(SelectCustomer)); }
        public string Report { get => GetValue(nameof(Report)); }
        public string Show { get => GetValue(nameof(Show)); }
        public string CustomerReport { get => GetValue(nameof(CustomerReport)); }
        public string Password { get => GetValue(nameof(Password)); }
        public string InvalidUsernameOrPassword { get => GetValue(nameof(InvalidUsernameOrPassword)); }
        public string UserNameAlreadyExists { get => GetValue(nameof(UserNameAlreadyExists)); }
        public string PasswordsMustBeIdentical { get => GetValue(nameof(PasswordsMustBeIdentical)); }
        public string SupervisorReport { get => GetValue(nameof(SupervisorReport)); }
        public string RepresentativeReport { get => GetValue(nameof(RepresentativeReport)); }
        public string JobOrderReport { get => GetValue(nameof(JobOrderReport)); }
        public string SelectSupervisor { get => GetValue(nameof(SelectSupervisor)); }
        public string SelectRepresentative { get => GetValue(nameof(SelectRepresentative)); }
        public string JobOrder { get => GetValue(nameof(JobOrder)); }
        public string SelectJobOrder { get => GetValue(nameof(SelectJobOrder)); }
    }

    public enum Language { Arabic, English }
}
