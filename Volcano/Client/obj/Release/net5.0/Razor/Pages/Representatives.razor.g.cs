#pragma checksum "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "68622eca6cf27046b9b0293bb6fa66584e71459d"
// <auto-generated/>
#pragma warning disable 1591
namespace Volcano.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Radzen;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Radzen.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client.Controls;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
           [Microsoft.AspNetCore.Authorization.Authorize]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/Representatives")]
    public partial class Representatives : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "h3");
            __builder.AddContent(1, 
#nullable restore
#line 9 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
     loc.Representatives

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(2, "\n\n");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "row toolbar");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "col-10");
            __builder.OpenComponent<Radzen.Blazor.RadzenButton>(7);
            __builder.AddAttribute(8, "Text", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 13 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                             loc.NewRepresentative

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(9, "Icon", "add_circle_outline");
            __builder.AddAttribute(10, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 13 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                      ShowNewRepresentativeForm

#line default
#line hidden
#nullable disable
            )));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\n\n");
            __builder.OpenComponent<Radzen.Blazor.RadzenDataGrid<Client.Representative>>(12);
            __builder.AddAttribute(13, "Count", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                        supervisorsCount

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(14, "Data", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<Client.Representative>>(
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                 representatives

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(15, "LoadData", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Radzen.LoadDataArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Radzen.LoadDataArgs>(this, 
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                             LoadData

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(16, "AllowSorting", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                                     false

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(17, "AllowFiltering", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 18 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                false

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(18, "AllowPaging", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 18 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                    true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(19, "PageSize", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 18 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                     PageSize

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(20, "ColumnWidth", "Auto");
            __builder.AddAttribute(21, "EmptyText", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 19 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                            loc.NoRows

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(22, "Columns", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.Representative>>(23);
                __builder2.AddAttribute(24, "Property", "Name");
                __builder2.AddAttribute(25, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 21 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                        false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(26, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 21 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                                       loc.Name

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(27, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.Representative>>(28);
                __builder2.AddAttribute(29, "Property", "Telephone");
                __builder2.AddAttribute(30, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 22 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                             false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(31, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 22 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                                            loc.Phone

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(32, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.Representative>>(33);
                __builder2.AddAttribute(34, "Property", "IsActive");
                __builder2.AddAttribute(35, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 23 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                            false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(36, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 23 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                                           loc.Active

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(37, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.Representative>>(38);
                __builder2.AddAttribute(39, "Width", "100px");
                __builder2.AddAttribute(40, "Template", (Microsoft.AspNetCore.Components.RenderFragment<Client.Representative>)((data) => (__builder3) => {
                    __builder3.OpenComponent<Radzen.Blazor.RadzenButton>(41);
                    __builder3.AddAttribute(42, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 26 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                      args => EditItem(data)

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(43, "title", 
#nullable restore
#line 26 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                      loc.Edit

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddAttribute(44, "ButtonStyle", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.ButtonStyle>(
#nullable restore
#line 26 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                             ButtonStyle.Light

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(45, "Icon", "edit");
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(46, "\n                ");
                    __builder3.OpenComponent<Radzen.Blazor.RadzenButton>(47);
                    __builder3.AddAttribute(48, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 27 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                      args => DeleteItem(data.Id)

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(49, "title", 
#nullable restore
#line 27 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                           loc.Delete

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddAttribute(50, "ButtonStyle", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.ButtonStyle>(
#nullable restore
#line 27 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
                                                                                                    ButtonStyle.Light

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(51, "Icon", "delete");
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 33 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\Representatives.razor"
        IEnumerable<Client.Representative> representatives;

            int supervisorsCount = 0;

            int PageSize = 30;

            async Task LoadData(Radzen.LoadDataArgs args = null)
            {
                representatives = await apiClient.GetRepresentativesAsync("", args?.Skip ?? 0, args?.Top ?? PageSize, session.SecToken);
                StateHasChanged();
            }

            protected override async Task OnInitializedAsync()
            {
                loc.CurrantLanguageChanged += (sender, language) => StateHasChanged();
                dialogService.OnClose += DialogClosed;

                supervisorsCount = await apiClient.GetRepresentativesCountAsync("", session.SecToken);
                await LoadData();

                await base.OnInitializedAsync();
            }

            async void ShowNewRepresentativeForm()
            {
                await dialogService.OpenAsync<Forms.EditRepresentative>(loc.NewRepresentative);
            }

            async void DeleteItem(Guid id)
            {
                bool? confirm = await dialogService.Confirm(loc.ConfirmMessage, loc.Confirm, new ConfirmOptions() { OkButtonText = loc.Yes, CancelButtonText = loc.No });
                if (confirm.Value == true)
                {
                    await apiClient.DeleteRepresentativeAsync(id, session.SecToken);
                    if (StaticData.StatusCode == "400")
                    {
                        notificationService.Notify(new NotificationMessage() { Summary = loc.CannotDelete, Severity = NotificationSeverity.Error });
                    }
                    else
                    {
                        await LoadData();
                    }
                }

            }

            async void DialogClosed(dynamic result)
            {
                // if (result is bool rez && rez == true)
                await LoadData();


                //if (((bool) result)==true)
                //{
                //    LoadData();
                //}
            }

            async void EditItem(Client.Representative representative)
            {
                Dictionary<string, object> parameters = new Dictionary<string, object>();
                parameters.Add("Representative", representative);
                await dialogService.OpenAsync<Forms.EditRepresentative>(loc.EditRepresentative, parameters);
            } 

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NotificationService notificationService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ClientSessionService session { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private LocalizerService loc { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private DialogService dialogService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private swaggerClient apiClient { get; set; }
    }
}
#pragma warning restore 1591
