#pragma checksum "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor" "{ff1816ec-aa5e-4d10-87f7-6f4963833460}" "724b0a13587d2e62e672b489ea7421a343d45e9e"
// <auto-generated/>
#pragma warning disable 1591
namespace Volcano.Client.Pages
{
    #line hidden
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Components;
#nullable restore
#line 1 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using System.Net.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using System.Net.Http.Json;

#line default
#line hidden
#nullable disable
#nullable restore
#line 3 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Forms;

#line default
#line hidden
#nullable disable
#nullable restore
#line 4 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Routing;

#line default
#line hidden
#nullable disable
#nullable restore
#line 5 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web;

#line default
#line hidden
#nullable disable
#nullable restore
#line 6 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.Web.Virtualization;

#line default
#line hidden
#nullable disable
#nullable restore
#line 7 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.AspNetCore.Components.WebAssembly.Http;

#line default
#line hidden
#nullable disable
#nullable restore
#line 8 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Microsoft.JSInterop;

#line default
#line hidden
#nullable disable
#nullable restore
#line 9 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client;

#line default
#line hidden
#nullable disable
#nullable restore
#line 10 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client.Shared;

#line default
#line hidden
#nullable disable
#nullable restore
#line 11 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Radzen;

#line default
#line hidden
#nullable disable
#nullable restore
#line 12 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Radzen.Blazor;

#line default
#line hidden
#nullable disable
#nullable restore
#line 14 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client.Services;

#line default
#line hidden
#nullable disable
#nullable restore
#line 15 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\_Imports.razor"
using Volcano.Client.Controls;

#line default
#line hidden
#nullable disable
#nullable restore
#line 2 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
           [Microsoft.AspNetCore.Authorization.Authorize]

#line default
#line hidden
#nullable disable
    [Microsoft.AspNetCore.Components.RouteAttribute("/JobOrders")]
    public partial class JobOrders : Microsoft.AspNetCore.Components.ComponentBase
    {
        #pragma warning disable 1998
        protected override void BuildRenderTree(Microsoft.AspNetCore.Components.Rendering.RenderTreeBuilder __builder)
        {
            __builder.OpenElement(0, "h3");
            __builder.AddContent(1, 
#nullable restore
#line 8 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
     loc.JobOrders

#line default
#line hidden
#nullable disable
            );
            __builder.CloseElement();
            __builder.AddMarkupContent(2, "\n\n");
            __builder.OpenElement(3, "div");
            __builder.AddAttribute(4, "class", "row toolbar");
            __builder.OpenElement(5, "div");
            __builder.AddAttribute(6, "class", "col-10");
            __builder.OpenComponent<Radzen.Blazor.RadzenButton>(7);
            __builder.AddAttribute(8, "Text", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 12 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                             loc.NewJobOrder

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(9, "Icon", "add_circle_outline");
            __builder.AddAttribute(10, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 12 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                ShowNewJobOrderForm

#line default
#line hidden
#nullable disable
            )));
            __builder.CloseComponent();
            __builder.CloseElement();
            __builder.CloseElement();
            __builder.AddMarkupContent(11, "\n\n");
            __builder.OpenComponent<Radzen.Blazor.RadzenDataGrid<Client.JobOrderRowModel>>(12);
            __builder.AddAttribute(13, "Count", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 16 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                        jobOrdersCount

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(14, "Data", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Collections.Generic.IEnumerable<Client.JobOrderRowModel>>(
#nullable restore
#line 16 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                               jobOrders

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(15, "LoadData", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Radzen.LoadDataArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Radzen.LoadDataArgs>(this, 
#nullable restore
#line 16 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                     LoadData

#line default
#line hidden
#nullable disable
            )));
            __builder.AddAttribute(16, "AllowSorting", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 16 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                             false

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(17, "AllowFiltering", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                false

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(18, "AllowPaging", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                    true

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(19, "PageSize", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Int32>(
#nullable restore
#line 17 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                     PageSize

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(20, "ColumnWidth", "Auto");
            __builder.AddAttribute(21, "EmptyText", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 18 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                            loc.NoRows

#line default
#line hidden
#nullable disable
            ));
            __builder.AddAttribute(22, "Columns", (Microsoft.AspNetCore.Components.RenderFragment)((__builder2) => {
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(23);
                __builder2.AddAttribute(24, "Property", "InvoiceNumber");
                __builder2.AddAttribute(25, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 20 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                   false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(26, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 20 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                  loc.InvoiceNumber

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(27, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(28);
                __builder2.AddAttribute(29, "Property", "CustomerName");
                __builder2.AddAttribute(30, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 21 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                  false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(31, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 21 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                 loc.Customer

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(32, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(33);
                __builder2.AddAttribute(34, "Property", "RepresentativeName");
                __builder2.AddAttribute(35, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 22 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                        false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(36, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 22 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                       loc.Representative

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(37, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(38);
                __builder2.AddAttribute(39, "Property", "SupervisorName");
                __builder2.AddAttribute(40, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 23 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                    false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(41, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 23 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                   loc.Supervisor

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(42, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(43);
                __builder2.AddAttribute(44, "Property", "ResponsibleName");
                __builder2.AddAttribute(45, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 24 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                     false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(46, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 24 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                    loc.ResponsibleName

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(47, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(48);
                __builder2.AddAttribute(49, "Property", "ResponsibleTelephone");
                __builder2.AddAttribute(50, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 25 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                          false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(51, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 25 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                         loc.ResponsiblePhone

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(52, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(53);
                __builder2.AddAttribute(54, "Property", "IsDone");
                __builder2.AddAttribute(55, "Filterable", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 26 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                            false

#line default
#line hidden
#nullable disable
                ));
                __builder2.AddAttribute(56, "Title", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.String>(
#nullable restore
#line 26 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                           loc.IsDone

#line default
#line hidden
#nullable disable
                ));
                __builder2.CloseComponent();
                __builder2.AddMarkupContent(57, "\n        ");
                __builder2.OpenComponent<Radzen.Blazor.RadzenDataGridColumn<Client.JobOrderRowModel>>(58);
                __builder2.AddAttribute(59, "Width", "100px");
                __builder2.AddAttribute(60, "Template", (Microsoft.AspNetCore.Components.RenderFragment<Client.JobOrderRowModel>)((data) => (__builder3) => {
                    __builder3.OpenComponent<Radzen.Blazor.RadzenButton>(61);
                    __builder3.AddAttribute(62, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 29 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                      args => EditItem(data.Id)

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(63, "title", 
#nullable restore
#line 29 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                         loc.Edit

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddAttribute(64, "ButtonStyle", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.ButtonStyle>(
#nullable restore
#line 29 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                ButtonStyle.Light

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(65, "Icon", "edit");
                    __builder3.AddAttribute(66, "Disabled", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 29 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                                           !session.CanEdit

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                    __builder3.AddMarkupContent(67, "\n                ");
                    __builder3.OpenComponent<Radzen.Blazor.RadzenButton>(68);
                    __builder3.AddAttribute(69, "Click", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Microsoft.AspNetCore.Components.EventCallback<Microsoft.AspNetCore.Components.Web.MouseEventArgs>>(Microsoft.AspNetCore.Components.EventCallback.Factory.Create<Microsoft.AspNetCore.Components.Web.MouseEventArgs>(this, 
#nullable restore
#line 30 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                      args => DeleteItem(data.Id)

#line default
#line hidden
#nullable disable
                    )));
                    __builder3.AddAttribute(70, "title", 
#nullable restore
#line 30 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                           loc.Delete

#line default
#line hidden
#nullable disable
                    );
                    __builder3.AddAttribute(71, "ButtonStyle", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<Radzen.ButtonStyle>(
#nullable restore
#line 30 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                    ButtonStyle.Light

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.AddAttribute(72, "Icon", "delete");
                    __builder3.AddAttribute(73, "Disabled", Microsoft.AspNetCore.Components.CompilerServices.RuntimeHelpers.TypeCheck<System.Boolean>(
#nullable restore
#line 30 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
                                                                                                                                                 !session.CanDelete

#line default
#line hidden
#nullable disable
                    ));
                    __builder3.CloseComponent();
                }
                ));
                __builder2.CloseComponent();
            }
            ));
            __builder.CloseComponent();
        }
        #pragma warning restore 1998
#nullable restore
#line 36 "C:\Users\Bassam-PC\Downloads\BassamSis-volcanoop-108c782eb041\Volcano\Client\Pages\JobOrders.razor"
        IEnumerable<Client.JobOrderRowModel> jobOrders;

    int jobOrdersCount = 0;

    int PageSize = 30;

    async Task LoadData(Radzen.LoadDataArgs args = null)
    {
        jobOrders = await apiClient.GetJobOrdersAsync("", args?.Skip ?? 0, args?.Top ?? PageSize, session.SecToken);
        StateHasChanged();
    }

    protected override async Task OnInitializedAsync()
    {
        loc.CurrantLanguageChanged += (sender, language) => StateHasChanged();
        dialogService.OnClose += DialogClosed;

        jobOrdersCount = await apiClient.GetJobOrdersCountAsync("", session.SecToken);
        await LoadData();

        await base.OnInitializedAsync();
    }

    async void ShowNewJobOrderForm()
    {
        await dialogService.OpenAsync<Forms.EditJobOrder>(loc.NewJobOrder, null, new DialogOptions { Width = "50%" });
    }

    async void DeleteItem(Guid id)
    {
        bool? confirm = await dialogService.Confirm(loc.ConfirmMessage, loc.Confirm, new ConfirmOptions() { OkButtonText = loc.Yes, CancelButtonText = loc.No });
        if (confirm.Value == true)
        {
            await apiClient.DeleteJobOrderAsync(id, session.SecToken);
            if (StaticData.StatusCode == "400")
            {
                notificationService.Notify(new NotificationMessage() { Summary = loc.CannotDelete, Severity = NotificationSeverity.Error });
            }
            else
            {
                await LoadData();
            }
        }
    }

    async void DialogClosed(dynamic result)
    {
        // if (result is bool rez && rez == true)
        await LoadData();


        //if (((bool) result)==true)
        //{
        //    LoadData();
        //}
    }

    async void EditItem(Guid id)
    {
        Dictionary<string, object> parameters = new Dictionary<string, object>();
        parameters.Add("JobOrder", await apiClient.GetJobOrderEditModelAsync(id, session.SecToken));
        await dialogService.OpenAsync<Forms.EditJobOrder>(loc.EditJobOrder, parameters, new DialogOptions { Width = "50%" });
    } 

#line default
#line hidden
#nullable disable
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private NotificationService notificationService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private ClientSessionService session { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private LocalizerService loc { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private DialogService dialogService { get; set; }
        [global::Microsoft.AspNetCore.Components.InjectAttribute] private swaggerClient apiClient { get; set; }
    }
}
#pragma warning restore 1591
