﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Volcano.Server.Migrations
{
    public partial class InitMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Devices",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NameEn = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Devices", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Representatives",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nationality = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    ResidencyNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResidencyDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    ResidencyEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    PassportNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PassportDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    PassportEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LicenseNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LicenseDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LicenseEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    BirthDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    WorkStartingDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Representatives", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Services",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NameEn = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Services", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Supervisors",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Nationality = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Address = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsActive = table.Column<bool>(type: "bit", nullable: false),
                    ResidencyNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResidencyDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    ResidencyEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    PassportNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    PassportDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    PassportEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LicenseNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    LicenseDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    LicenseEndDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    BirthDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true),
                    WorkStartingDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Supervisors", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    NameAr = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    NameEn = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    RepresentativeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ResponsibleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    IsCustomer = table.Column<bool>(type: "bit", nullable: false),
                    Email = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Telephone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    StartingDate = table.Column<DateTimeOffset>(type: "datetimeoffset", nullable: false),
                    Notes = table.Column<string>(type: "nvarchar(max)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Customers_Representatives_RepresentativeId",
                        column: x => x.RepresentativeId,
                        principalTable: "Representatives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "JobOrders",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    InvoiceNumber = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    RepresentativeId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    SupervisorId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    ResponsibleName = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ResponsibleTelephone = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    SpareParts = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Statement = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    Done = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    ReportPreparing = table.Column<bool>(type: "bit", nullable: false),
                    MaintenanceOfExtinguishers = table.Column<bool>(type: "bit", nullable: false),
                    MaintenanceAndRepair = table.Column<bool>(type: "bit", nullable: false),
                    RepairFailures = table.Column<bool>(type: "bit", nullable: false),
                    ApproveSparePartsChange = table.Column<bool>(type: "bit", nullable: false),
                    SiteStudy = table.Column<bool>(type: "bit", nullable: false),
                    IsDone = table.Column<bool>(type: "bit", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_JobOrders", x => x.Id);
                    table.ForeignKey(
                        name: "FK_JobOrders_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobOrders_Representatives_RepresentativeId",
                        column: x => x.RepresentativeId,
                        principalTable: "Representatives",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_JobOrders_Supervisors_SupervisorId",
                        column: x => x.SupervisorId,
                        principalTable: "Supervisors",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "OperationPlans",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uniqueidentifier", nullable: false),
                    ServiceId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    DeviceId = table.Column<Guid>(type: "uniqueidentifier", nullable: true),
                    M1 = table.Column<bool>(type: "bit", nullable: false),
                    M2 = table.Column<bool>(type: "bit", nullable: false),
                    M3 = table.Column<bool>(type: "bit", nullable: false),
                    M4 = table.Column<bool>(type: "bit", nullable: false),
                    M5 = table.Column<bool>(type: "bit", nullable: false),
                    M6 = table.Column<bool>(type: "bit", nullable: false),
                    M7 = table.Column<bool>(type: "bit", nullable: false),
                    M8 = table.Column<bool>(type: "bit", nullable: false),
                    M9 = table.Column<bool>(type: "bit", nullable: false),
                    M10 = table.Column<bool>(type: "bit", nullable: false),
                    M11 = table.Column<bool>(type: "bit", nullable: false),
                    M12 = table.Column<bool>(type: "bit", nullable: false),
                    CustomerId = table.Column<Guid>(type: "uniqueidentifier", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_OperationPlans", x => x.Id);
                    table.ForeignKey(
                        name: "FK_OperationPlans_Customers_CustomerId",
                        column: x => x.CustomerId,
                        principalTable: "Customers",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OperationPlans_Devices_DeviceId",
                        column: x => x.DeviceId,
                        principalTable: "Devices",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                    table.ForeignKey(
                        name: "FK_OperationPlans_Services_ServiceId",
                        column: x => x.ServiceId,
                        principalTable: "Services",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Customers_RepresentativeId",
                table: "Customers",
                column: "RepresentativeId");

            migrationBuilder.CreateIndex(
                name: "IX_JobOrders_CustomerId",
                table: "JobOrders",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_JobOrders_RepresentativeId",
                table: "JobOrders",
                column: "RepresentativeId");

            migrationBuilder.CreateIndex(
                name: "IX_JobOrders_SupervisorId",
                table: "JobOrders",
                column: "SupervisorId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationPlans_CustomerId",
                table: "OperationPlans",
                column: "CustomerId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationPlans_DeviceId",
                table: "OperationPlans",
                column: "DeviceId");

            migrationBuilder.CreateIndex(
                name: "IX_OperationPlans_ServiceId",
                table: "OperationPlans",
                column: "ServiceId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "JobOrders");

            migrationBuilder.DropTable(
                name: "OperationPlans");

            migrationBuilder.DropTable(
                name: "Supervisors");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Devices");

            migrationBuilder.DropTable(
                name: "Services");

            migrationBuilder.DropTable(
                name: "Representatives");
        }
    }
}
