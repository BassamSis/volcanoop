﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Volcano.Server.Migrations
{
    public partial class AddBatchNumber2JobOrders : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BatchNumber",
                table: "JobOrders",
                type: "int",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BatchNumber",
                table: "JobOrders");
        }
    }
}
