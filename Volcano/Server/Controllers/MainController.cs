﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volcano.Server.Data;
using Microsoft.EntityFrameworkCore;
using System.Security.Claims;
using Volcano.Server.Models;
using Microsoft.Data.SqlClient;


namespace Volcano.Server.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class MainController : ControllerBase
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly Services.SessionService sessionService;

        public MainController(ApplicationDbContext applicationDbContext, Services.SessionService sessionService)
        {
            _dbContext = applicationDbContext;
            this.sessionService = sessionService;

            // Set default admin account
            if (_dbContext.Users.Count() == 1)
            {
                var user = _dbContext.Users.FirstOrDefault();
                user.IsActive = true;
                user.IsAdmin = true;
                user.CanEdit = true;
                user.CanDelete = true;

                _dbContext.SaveChanges();
            }
        }
        /*
         
        [NonAction]
        async Task<bool> IsAdmin(Guid secToken)
        {
            //if (!User.Identity.IsAuthenticated) return false;

            //var userId = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault();

            //Models.ApplicationUser user = await _dbContext.Users.Where(x => x.Id == userId).FirstOrDefaultAsync();
            //if (user.IsAdmin) return true;

            //return false;
        }

        [NonAction]
        async Task<bool> CanEdit(Guid secToken)
        {
            //if (!User.Identity.IsAuthenticated) return false;

            //var userId = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault();

            //Models.ApplicationUser user = await _dbContext.Users.Where(x => x.Id == userId).FirstOrDefaultAsync();
            //if (user.IsAdmin) return true;
            //if (user.CanEdit) return true;

            //return false;
        }

        [NonAction]
        async Task<bool> CanDelete(Guid secToken)
        {
            //if (!User.Identity.IsAuthenticated) return false;

            //var userId = User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault();

            //Models.ApplicationUser user = await _dbContext.Users.Where(x => x.Id == userId).FirstOrDefaultAsync();
            //if (user.IsAdmin) return true;
            //if (user.CanDelete) return true;

            //return false;
        }

        */

        [HttpGet("GetDevicesCount")]
        public async Task<ActionResult<int>> GetDevicesCount(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Devices.CountAsync();
            else return await _dbContext.Devices.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).CountAsync();
        }

        [HttpGet("GetAllDevices")]
        public async Task<ActionResult<IEnumerable<Device>>> GetAllDevices(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Devices.ToArrayAsync();
            else
                return await _dbContext.Devices.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToArrayAsync();
        }

        [HttpGet("GetDevices")]
        public async Task<ActionResult<IEnumerable<Device>>> GetDevices(string search, int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Devices.Skip(skip).Take(top).ToArrayAsync();
            else
                return await _dbContext.Devices.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Skip(skip).Take(top).ToArrayAsync();
        }

        [HttpGet("GetAllDevicesNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetAllDevicesNames([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Devices.Select(x => new NamedItem { Id = x.Id, Name = $"{x.NameAr}({x.NameEn})" }).ToArrayAsync();
        }

        [HttpGet("GetDevicesNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetDevicesNames(int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Devices.Skip(skip).Take(top).Select(x => new NamedItem { Id = x.Id, Name = $"{x.NameAr}({x.NameEn})" }).ToArrayAsync();
        }

        [HttpGet("GetDevice")]
        public async Task<ActionResult<Device>> GetDevice(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Devices.FirstOrDefaultAsync(x => x.Id == id);
        }


        [HttpPost("CreateDevice")]
        public async Task<ActionResult<Device>> CreateDevice([FromBody] Device device, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (ModelState.IsValid)
            {
                //_dbContext.Devices.Add(new Device()
                //{
                //    NameAr = device.NameAr,
                //    NameEn = device.NameEn
                //});
                _dbContext.Devices.Add(device);
                await _dbContext.SaveChangesAsync();
            }

            return device;
        }


        [HttpPut("EditDevice")]
        public async Task<ActionResult<Device>> EditDevice([FromQuery] Guid id, [FromBody] Device device, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanEdit) return Unauthorized();

            if (ModelState.IsValid)
            {
                var dev = await _dbContext.Devices.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (dev == null) return NotFound();

                dev.NameAr = device.NameAr;
                dev.NameEn = device.NameEn;

                await _dbContext.SaveChangesAsync();
            }

            return device;
        }

        [HttpDelete("DeleteDevice")]
        public async Task<IActionResult> DeleteDevice(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanDelete) return Unauthorized();

            var dev = await _dbContext.Devices.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (dev == null) return NotFound();

            _dbContext.Devices.Remove(dev);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        //-- services --
        [HttpGet("GetServicesCount")]
        public async Task<ActionResult<int>> GetServicesCount(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Services.CountAsync();
            else return await _dbContext.Services.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).CountAsync();
        }

        [HttpGet("GetAllServices")]
        public async Task<ActionResult<IEnumerable<Service>>> GetAllServices(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Services.ToArrayAsync();
            else
                return await _dbContext.Services.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToArrayAsync();
        }

        [HttpGet("GetServices")]
        public async Task<ActionResult<IEnumerable<Service>>> GetServices(string search, [FromQuery] int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Services.Skip(skip).Take(top).ToArrayAsync();
            else
                return await _dbContext.Services.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Skip(skip).Take(top).ToArrayAsync();
        }

        [HttpGet("GetAllServicesNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetAllServicesNames([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Services.Select(x => new NamedItem { Id = x.Id, Name = $"{x.NameAr}({x.NameEn})" }).ToArrayAsync();
        }

        [HttpGet("GetServicesNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetServicesNames(int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Services.Skip(skip).Take(top).Select(x => new NamedItem { Id = x.Id, Name = $"{x.NameAr}({x.NameEn})" }).ToArrayAsync();
        }

        [HttpGet("GetService")]
        public async Task<ActionResult<Service>> GetService(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Services.FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPost("CreateService")]
        public async Task<ActionResult<Service>> CreateService([FromBody] Service service, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (ModelState.IsValid)
            {
                //_dbContext.Devices.Add(new Device()
                //{
                //    NameAr = device.NameAr,
                //    NameEn = device.NameEn
                //});
                _dbContext.Services.Add(service);
                await _dbContext.SaveChangesAsync();
            }

            return service;
        }


        [HttpPut("EditService")]
        public async Task<ActionResult<Service>> EditService([FromQuery] Guid id, [FromBody] Service service, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanEdit) return Unauthorized();

            if (ModelState.IsValid)
            {
                var ser = await _dbContext.Services.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (ser == null) return NotFound();

                ser.NameAr = service.NameAr;
                ser.NameEn = service.NameEn;

                await _dbContext.SaveChangesAsync();
            }

            return service;
        }

        [HttpDelete("DeleteService")]
        public async Task<IActionResult> DeleteService(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanDelete) return Unauthorized();

            var ser = await _dbContext.Services.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (ser == null) return NotFound();

            _dbContext.Services.Remove(ser);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        // -- Supervisor

        [HttpGet("GetSupervisorsCount")]
        public async Task<ActionResult<int>> GetSupervisorsCount(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Supervisors.CountAsync();
            else return await _dbContext.Supervisors.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).CountAsync();
        }

        [HttpGet("GetAllSupervisors")]
        public async Task<ActionResult<IEnumerable<Supervisor>>> GetAllSupervisors(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Supervisors.ToArrayAsync();
            else
                return await _dbContext.Supervisors.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToArrayAsync();
        }

        [HttpGet("GetSupervisors")]
        public async Task<ActionResult<IEnumerable<Supervisor>>> GetSupervisors(string search, [FromQuery] int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Supervisors.Skip(skip).Take(top).ToArrayAsync();
            else
                return await _dbContext.Supervisors.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Skip(skip).Take(top).ToArrayAsync();
        }

        [HttpGet("GetAllSupervisorsNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetAllSupervisorsNames([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Supervisors.Select(x => new NamedItem { Id = x.Id, Name = x.Name }).ToArrayAsync();
        }

        [HttpGet("GetSupervisorsNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetSupervisorsNames(int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Supervisors.Skip(skip).Take(top).Select(x => new NamedItem { Id = x.Id, Name = x.Name }).ToArrayAsync();
        }

        [HttpGet("GetSupervisor")]
        public async Task<ActionResult<Supervisor>> GetSupervisor(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Supervisors.FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPost("CreateSupervisor")]
        public async Task<ActionResult<Supervisor>> CreateSupervisor([FromBody] Supervisor supervisor, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (ModelState.IsValid)
            {
                //_dbContext.Devices.Add(new Device()
                //{
                //    NameAr = device.NameAr,
                //    NameEn = device.NameEn
                //});
                _dbContext.Supervisors.Add(supervisor);
                await _dbContext.SaveChangesAsync();
            }

            return supervisor;
        }

        [HttpPut("EditSupervisor")]
        public async Task<ActionResult<Supervisor>> EditSupervisor([FromQuery] Guid id, [FromBody] Supervisor supervisor, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanEdit) return Unauthorized();

            if (ModelState.IsValid)
            {
                var sub = await _dbContext.Supervisors.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (sub == null) return NotFound();

                sub.Name = supervisor.Name;
                sub.Address = supervisor.Address;
                sub.BirthDate = supervisor.BirthDate;
                sub.IsActive = supervisor.IsActive;
                sub.LicenseDate = supervisor.LicenseDate;
                sub.LicenseEndDate = supervisor.LicenseEndDate;
                sub.LicenseNumber = supervisor.LicenseNumber;
                sub.Nationality = supervisor.Nationality;
                sub.PassportDate = supervisor.PassportDate;
                sub.PassportEndDate = supervisor.PassportEndDate;
                sub.PassportNumber = supervisor.PassportNumber;
                sub.ResidencyDate = supervisor.ResidencyDate;
                sub.ResidencyEndDate = supervisor.ResidencyEndDate;
                sub.ResidencyNumber = supervisor.ResidencyNumber;
                sub.Telephone = supervisor.Telephone;
                sub.WorkStartingDate = supervisor.WorkStartingDate;

                await _dbContext.SaveChangesAsync();
            }

            return supervisor;
        }

        [HttpDelete("DeleteSupervisor")]
        public async Task<IActionResult> DeleteSupervisor(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanDelete) return Unauthorized();

            var sup = await _dbContext.Supervisors.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (sup == null) return NotFound();

            _dbContext.Supervisors.Remove(sup);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        // -- Representative

        [HttpGet("GetRepresentativesCount")]
        public async Task<ActionResult<int>> GetRepresentativesCount(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Representatives.CountAsync();
            else return await _dbContext.Representatives.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).CountAsync();
        }

        [HttpGet("GetAllRepresentatives")]
        public async Task<ActionResult<IEnumerable<Representative>>> GetAllRepresentatives(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Representatives.ToArrayAsync();
            else
                return await _dbContext.Representatives.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToArrayAsync();
        }

        [HttpGet("GetRepresentatives")]
        public async Task<ActionResult<IEnumerable<Representative>>> GetRepresentatives(string search, [FromQuery] int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Representatives.Skip(skip).Take(top).ToArrayAsync();
            else
                return await _dbContext.Representatives.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).Skip(skip).Take(top).ToArrayAsync();
        }

        [HttpGet("GetAllRepresentativesNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetAllRepresentativesNames([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Representatives.Select(x => new NamedItem { Id = x.Id, Name = x.Name }).ToArrayAsync();
        }

        [HttpGet("GetRepresentativesNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetRepresentativesNames(int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Representatives.Skip(skip).Take(top).Select(x => new NamedItem { Id = x.Id, Name = x.Name }).ToArrayAsync();
        }

        [HttpGet("GetRepresentative")]
        public async Task<ActionResult<Representative>> GetRepresentative(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Representatives.FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPost("CreateRepresentative")]
        public async Task<ActionResult<Representative>> CreateRepresentative([FromBody] Representative representative, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (ModelState.IsValid)
            {
                _dbContext.Representatives.Add(representative);
                await _dbContext.SaveChangesAsync();
            }

            return representative;
        }

        [HttpPut("EditRepresentative")]
        public async Task<ActionResult<Representative>> EditRepresentative([FromQuery] Guid id, [FromBody] Representative jobOrder, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanEdit) return Unauthorized();

            if (ModelState.IsValid)
            {
                var rep = await _dbContext.Representatives.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (rep == null) return NotFound();

                rep.Name = jobOrder.Name;
                rep.Address = jobOrder.Address;
                rep.BirthDate = jobOrder.BirthDate;
                rep.IsActive = jobOrder.IsActive;
                rep.LicenseDate = jobOrder.LicenseDate;
                rep.LicenseEndDate = jobOrder.LicenseEndDate;
                rep.LicenseNumber = jobOrder.LicenseNumber;
                rep.Nationality = jobOrder.Nationality;
                rep.PassportDate = jobOrder.PassportDate;
                rep.PassportEndDate = jobOrder.PassportEndDate;
                rep.PassportNumber = jobOrder.PassportNumber;
                rep.ResidencyDate = jobOrder.ResidencyDate;
                rep.ResidencyEndDate = jobOrder.ResidencyEndDate;
                rep.ResidencyNumber = jobOrder.ResidencyNumber;
                rep.Telephone = jobOrder.Telephone;
                rep.WorkStartingDate = jobOrder.WorkStartingDate;

                await _dbContext.SaveChangesAsync();
            }

            return jobOrder;
        }

        [HttpDelete("DeleteRepresentative")]
        public async Task<IActionResult> DeleteRepresentative(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanDelete) return Unauthorized();

            var rep = await _dbContext.Representatives.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (rep == null) return NotFound();

            _dbContext.Representatives.Remove(rep);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        // -- JobOrder --

        [HttpGet("GetJobOrdersCount")]
        public async Task<ActionResult<int>> GetJobOrdersCount(string search, [FromQuery] Guid secToken)//TODO: Fix Search!
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            //if (String.IsNullOrWhiteSpace(search))
            return await _dbContext.JobOrders.CountAsync();
            //else return await _dbContext.JobOrders.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).CountAsync();
        }

        [HttpGet("GetAllJobOrders")]
        public async Task<ActionResult<IEnumerable<JobOrder>>> GetAllJobOrders(string search, [FromQuery] Guid secToken)//TODO: Fix Search!
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            //if (String.IsNullOrWhiteSpace(search))
            return await _dbContext.JobOrders.ToArrayAsync();
            //else
            //  return await _dbContext.JobOrders.Where(x => x.Name.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.LicenseNumber.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToArrayAsync();
        }

        [HttpGet("GetJobOrders")]
        public async Task<ActionResult<IEnumerable<JobOrderRowModel>>> GetJobOrders(string search, [FromQuery] int skip, [FromQuery] int top, [FromQuery] Guid secToken)//TODO: Fix Search!
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            //if (String.IsNullOrWhiteSpace(search))
            return await _dbContext.JobOrders.Include(x => x.Customer).Include(x => x.Representative).Include(x => x.Supervisor).Skip(skip).Take(top).Select(x => new JobOrderRowModel
            {
                CustomerName = x.Customer.NameAr,
                Id = x.Id,
                InvoiceNumber = x.InvoiceNumber,
                IsDone = x.IsDone,
                RepresentativeName = x.Representative.Name,
                ResponsibleName = x.ResponsibleName,
                ResponsibleTelephone = x.ResponsibleTelephone,
                SupervisorName = x.Supervisor.Name

            }).ToArrayAsync();
        }

        [HttpGet("GetAllJobOrdersNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetAllJobOrdersNames([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.JobOrders.Include(z => z.Customer).Select(x => new NamedItem { Id = x.Id, Name = $"Customer={x.Customer.NameAr}, Invoice={x.InvoiceNumber}" }).ToArrayAsync();
        }

        [HttpGet("GetJobOrdersNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetJobOrdersNames(int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.JobOrders.Include(z => z.Customer).Skip(skip).Take(top).Select(x => new NamedItem { Id = x.Id, Name = $"Customer={x.Customer.NameAr}, Invoice={x.InvoiceNumber}" }).ToArrayAsync();
        }

        [HttpGet("GetJobOrder")]
        public async Task<ActionResult<JobOrder>> GetJobOrder(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.JobOrders.Include(x => x.Customer).Include(x => x.Representative).Include(x => x.Supervisor).FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPost("CreateJobOrder")]
        public async Task<ActionResult<JobOrderEditModel>> CreateJobOrder([FromBody] JobOrderEditModel jobOrder, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (ModelState.IsValid)
            {

                string statment = @"INSERT INTO [JobOrders] ([Id],
[InvoiceNumber]  ,            
[CustomerId]      ,           
[RepresentativeId] ,          
[SupervisorId]      ,         
[ResponsibleName]    ,        
[ResponsibleTelephone],       
[SpareParts]           ,      
[Statement]             ,     
[Done]                   ,    
[ReportPreparing]         ,   
[MaintenanceOfExtinguishers] ,
[MaintenanceAndRepair]       ,
[RepairFailures]             ,
[ApproveSparePartsChange]    ,
[SiteStudy]                  ,
[IsDone]        ,[BatchNumber]             )VALUES(@Id,
@InvoiceNumber  ,            
@CustomerId    ,           
@RepresentativeId ,          
@SupervisorId      ,         
@ResponsibleName    ,        
@ResponsibleTelephone,       
@SpareParts           ,      
@Statement             ,     
@Done               ,    
@ReportPreparing,   
@MaintenanceOfExtinguishers ,
@MaintenanceAndRepair  ,
@RepairFailures        ,
@ApproveSparePartsChange,
@SiteStudy     ,
@IsDone    ,@BatchNumber       );";

                Microsoft.Data.SqlClient.SqlParameter[] parameters = {
                    new Microsoft.Data.SqlClient.SqlParameter("@Id", Guid.NewGuid()),
                    new Microsoft.Data.SqlClient.SqlParameter("@InvoiceNumber", jobOrder.InvoiceNumber),
                    new Microsoft.Data.SqlClient.SqlParameter("@CustomerId", jobOrder.CustomerId),
                    new Microsoft.Data.SqlClient.SqlParameter("@RepresentativeId", jobOrder.RepresentativeId),
                    new Microsoft.Data.SqlClient.SqlParameter("@SupervisorId", jobOrder.SupervisorId),
                    new Microsoft.Data.SqlClient.SqlParameter("@ResponsibleName", jobOrder.ResponsibleName),
                    new Microsoft.Data.SqlClient.SqlParameter("@ResponsibleTelephone", jobOrder.ResponsibleTelephone),
                    new Microsoft.Data.SqlClient.SqlParameter("@SpareParts", jobOrder.SpareParts),
                    new Microsoft.Data.SqlClient.SqlParameter("@Statement", jobOrder.Statement),
                    new Microsoft.Data.SqlClient.SqlParameter("@Done", jobOrder.Done),
                    new Microsoft.Data.SqlClient.SqlParameter("@ReportPreparing", jobOrder.ReportPreparing),
                    new Microsoft.Data.SqlClient.SqlParameter("@MaintenanceOfExtinguishers", jobOrder.MaintenanceOfExtinguishers),
                    new Microsoft.Data.SqlClient.SqlParameter("@MaintenanceAndRepair", jobOrder.MaintenanceAndRepair),
                    new Microsoft.Data.SqlClient.SqlParameter("@RepairFailures", jobOrder.RepairFailures),
                    new Microsoft.Data.SqlClient.SqlParameter("@ApproveSparePartsChange", jobOrder.ApproveSparePartsChange),
                    new Microsoft.Data.SqlClient.SqlParameter("@SiteStudy", jobOrder.SiteStudy),
                    new Microsoft.Data.SqlClient.SqlParameter("@IsDone", jobOrder.IsDone),
                    new Microsoft.Data.SqlClient.SqlParameter("@BatchNumber", jobOrder.BatchNumber==0?_dbContext.JobOrders.Count()+1:jobOrder.BatchNumber),
                };
                _dbContext.Database.ExecuteSqlRaw(statment, parameters);
            }

            return jobOrder;
        }

        [HttpGet("GetJobOrderEditModel")]
        public async Task<ActionResult<JobOrderEditModel>> GetJobOrderEditModel(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.JobOrders.Select(x => new JobOrderEditModel
            {
                ApproveSparePartsChange = x.ApproveSparePartsChange,
                CustomerId = x.Customer.Id,
                Done = x.Done,
                Id = x.Id,
                InvoiceNumber = x.InvoiceNumber,
                IsDone = x.IsDone,
                MaintenanceAndRepair = x.MaintenanceAndRepair,
                MaintenanceOfExtinguishers = x.MaintenanceOfExtinguishers,
                RepairFailures = x.RepairFailures,
                ReportPreparing = x.ReportPreparing,
                RepresentativeId = x.Representative.Id,
                ResponsibleName = x.ResponsibleName,
                ResponsibleTelephone = x.ResponsibleTelephone,
                SiteStudy = x.SiteStudy,
                SpareParts = x.SpareParts,
                Statement = x.Statement,
                SupervisorId = x.Supervisor.Id
            }).FirstOrDefaultAsync(x => x.Id == id);
        }

        [HttpPut("EditJobOrder")]
        public async Task<ActionResult<JobOrderEditModel>> EditJobOrder([FromQuery] Guid id, [FromBody] JobOrderEditModel jobOrder, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanEdit) return Unauthorized();

            if (ModelState.IsValid)
            {
                var job = await _dbContext.JobOrders.Where(x => x.Id == id).FirstOrDefaultAsync();
                if (job == null) return NotFound();

                string statment = @"UPDATE [JobOrders] SET 
[InvoiceNumber] =@InvoiceNumber  ,            
[CustomerId] =@CustomerId     ,           
[RepresentativeId] =@RepresentativeId,          
[SupervisorId]=@SupervisorId      ,         
[ResponsibleName] =@ResponsibleName   ,        
[ResponsibleTelephone]=@ResponsibleTelephone,       
[SpareParts] =@SpareParts          ,      
[Statement]  =@Statement           ,     
[Done]   =@Done                ,    
[ReportPreparing]  =@ReportPreparing       ,   
[MaintenanceOfExtinguishers]=@MaintenanceOfExtinguishers ,
[MaintenanceAndRepair] =@MaintenanceAndRepair      ,
[RepairFailures]  =@RepairFailures           ,
[ApproveSparePartsChange]=@ApproveSparePartsChange    ,
[SiteStudy]  =@SiteStudy WHERE [Id]=@Id;";

                SqlParameter[] parameters = {
                    new SqlParameter("@InvoiceNumber", jobOrder.InvoiceNumber),
                    new SqlParameter("@CustomerId", jobOrder.CustomerId),
                    new SqlParameter("@RepresentativeId", jobOrder.RepresentativeId),
                    new SqlParameter("@SupervisorId", jobOrder.SupervisorId),
                    new SqlParameter("@ResponsibleName", jobOrder.ResponsibleName),
                    new SqlParameter("@ResponsibleTelephone", jobOrder.ResponsibleTelephone),
                    new SqlParameter("@SpareParts", jobOrder.SpareParts),
                    new SqlParameter("@Statement", jobOrder.Statement),
                    new SqlParameter("@Done", jobOrder.Done),
                    new SqlParameter("@ReportPreparing", jobOrder.ReportPreparing),
                    new SqlParameter("@MaintenanceOfExtinguishers", jobOrder.MaintenanceOfExtinguishers),
                    new SqlParameter("@MaintenanceAndRepair", jobOrder.MaintenanceAndRepair),
                    new SqlParameter("@RepairFailures", jobOrder.RepairFailures),
                    new SqlParameter("@ApproveSparePartsChange", jobOrder.ApproveSparePartsChange),
                    new SqlParameter("@SiteStudy", jobOrder.SiteStudy),
                    new SqlParameter("@IsDone", jobOrder.IsDone),
                    new SqlParameter("@Id", jobOrder.Id),
                };
                _dbContext.Database.ExecuteSqlRaw(statment, parameters);

            }

            return jobOrder;
        }

        [HttpDelete("DeleteJobOrder")]
        public async Task<IActionResult> DeleteJobOrder(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanDelete) return Unauthorized();

            var job = await _dbContext.JobOrders.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (job == null) return NotFound();

            _dbContext.JobOrders.Remove(job);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpGet("GetCustomersCount")]
        public async Task<ActionResult<int>> GetCustomersCount(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Customers.CountAsync();
            else return await _dbContext.Customers.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).CountAsync();
        }

        [HttpGet("GetAllCustomers")]
        public async Task<ActionResult<IEnumerable<Customer>>> GetAllCustomers(string search, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (String.IsNullOrWhiteSpace(search))
                return await _dbContext.Customers.ToArrayAsync();
            else
                return await _dbContext.Customers.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase)).ToArrayAsync();
        }

        [HttpGet("GetCustomers")]
        public async Task<ActionResult<IEnumerable<CustomerRowModel>>> GetCustomers(string search, [FromQuery] int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            IQueryable<Customer> query = _dbContext.Customers.Include(x => x.Representative).Include(x => x.Plans);

            if (String.IsNullOrWhiteSpace(search))
                query.Where(x => x.NameAr.Contains(search, StringComparison.InvariantCultureIgnoreCase) || x.NameEn.Contains(search, StringComparison.InvariantCultureIgnoreCase));

            query = query.Skip(skip).Take(top);

            var result = await query.Select(x => new CustomerRowModel { Id = x.Id, IsCustomer = x.IsCustomer, NameAr = x.NameAr, NameEn = x.NameEn, RepresentativeName = x.Representative.Name, ResponsibleName = x.ResponsibleName }).ToArrayAsync();

            return result;
        }

        [HttpGet("GetAllCustomersNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetAllCustomersNames([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Customers.Select(x => new NamedItem { Id = x.Id, Name = $"{x.NameAr}({x.NameEn})" }).ToArrayAsync();
        }

        [HttpGet("GetCustomersNames")]
        public async Task<ActionResult<IEnumerable<NamedItem>>> GetCustomersNames(int skip, [FromQuery] int top, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Customers.Skip(skip).Take(top).Select(x => new NamedItem { Id = x.Id, Name = $"{x.NameAr}({x.NameEn})" }).ToArrayAsync();
        }

        [HttpGet("GetCustomer")]
        public async Task<ActionResult<Customer>> GetCustomer(Guid customerId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            var customer = await _dbContext.Customers.Include(x => x.Representative)
                .Include(x => x.Plans).ThenInclude(x => x.Service)
                .Include(x => x.Plans).ThenInclude(x => x.Device)

                .FirstOrDefaultAsync(x => x.Id == customerId);

            return customer;
        }

        [HttpGet("GetCustomerEdit")]
        public async Task<ActionResult<CustomerInputModel>> GetCustomerEdit(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            var result = await _dbContext.Customers.Where(x => x.Id == id).Select(x => new CustomerInputModel
            {
                Email = x.Email,
                IsCustomer = x.IsCustomer,
                NameAr = x.NameAr,
                NameEn = x.NameEn,
                Notes = x.Notes,
                Plans = x.Plans,
                RepresentativeId = x.Representative.Id,
                ResponsibleName = x.ResponsibleName,
                StartingDate = x.StartingDate,
                Telephone = x.Telephone
            }).FirstOrDefaultAsync();

            return result;
        }

        [HttpPost("CreateCustomer")]
        public async Task<ActionResult<Customer>> CreateCustomer([FromBody] Customer customer, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (ModelState.IsValid)
            {
                customer.Representative = await _dbContext.Representatives.FirstOrDefaultAsync(x => x.Id == customer.Representative.Id);

                List<(bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, Guid, Guid)> plans = new List<(bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, Guid, Guid)>();

                if (customer.Plans != null && customer.Plans.Count > 0)
                    foreach (var p in customer.Plans)
                    {
                        plans.Add((p.M1, p.M2, p.M3, p.M4, p.M5, p.M6, p.M7, p.M8, p.M9, p.M10, p.M11, p.M12, p.Service.Id, p.Device.Id));
                    }

                customer.Plans.Clear();
                _dbContext.Customers.Add(customer);
                await _dbContext.SaveChangesAsync();

                customer.Plans.Clear();
                _dbContext.Customers.Update(customer);

                await _dbContext.SaveChangesAsync();


                // save plans:

                await _dbContext.Database.BeginTransactionAsync();

                // _dbContext.Database.ExecuteSqlRaw("DELETE FROM [OperationPlans] WHERE [CustomerId]=@CustomerId;", new SqlParameter[] { new SqlParameter("@CustomerId", customer.Id) });

                foreach (var item in plans)
                {
                    _dbContext.Database.ExecuteSqlRaw(@"INSERT INTO [OperationPlans] ([Id],
[ServiceId], 
[DeviceId],  
[M1],        
[M2],        
[M3],        
[M4],        
[M5],        
[M6],        
[M7],        
[M8],        
[M9],        
[M10],       
[M11],       
[M12],       
[CustomerId]) VALUES (@Id,
@ServiceId, 
@DeviceId,  
@M1,        
@M2,        
@M3,        
@M4,        
@M5,        
@M6,        
@M7,        
@M8,        
@M9,        
@M10,       
@M11,       
@M12,       
@CustomerId);", new SqlParameter[] {
new SqlParameter("@Id",Guid.NewGuid()),
new SqlParameter("@M1",item.Item1),
new SqlParameter("@M2",item.Item2),
new SqlParameter("@M3",item.Item3),
new SqlParameter("@M4",item.Item4),
new SqlParameter("@M5",item.Item5),
new SqlParameter("@M6",item.Item6),
new SqlParameter("@M7",item.Item7),
new SqlParameter("@M8",item.Item8),
new SqlParameter("@M9",item.Item9),
new SqlParameter("@M10",item.Item10),
new SqlParameter("@M11",item.Item11),
new SqlParameter("@M12",item.Item12),
new SqlParameter("@ServiceId",item.Item13),
new SqlParameter("@DeviceId",item.Item14),
new SqlParameter("@CustomerId",customer.Id),
                    });
                }

                await _dbContext.Database.CommitTransactionAsync();

                return customer;
            }
            else return BadRequest();
        }

        [HttpPut("EditCustomer")]
        public async Task<ActionResult<Customer>> EditCustomer([FromQuery] Guid id, [FromBody] Customer customer, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanEdit) return Unauthorized();

            if (ModelState.IsValid)
            {

                //  M1, M2,...,ServiceId, DeviceId
                List<(bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, Guid, Guid)> plans = new List<(bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, bool, Guid, Guid)>();

                if (customer.Plans != null && customer.Plans.Count > 0)
                    foreach (var p in customer.Plans)
                    {
                        plans.Add((p.M1, p.M2, p.M3, p.M4, p.M5, p.M6, p.M7, p.M8, p.M9, p.M10, p.M11, p.M12, p.Service.Id, p.Device.Id));
                    }

                customer.Plans.Clear();
                _dbContext.Customers.Update(customer);

                await _dbContext.SaveChangesAsync();


                // save plans:

                await _dbContext.Database.BeginTransactionAsync();

                _dbContext.Database.ExecuteSqlRaw("DELETE FROM [OperationPlans] WHERE [CustomerId]=@CustomerId;", new SqlParameter[] { new SqlParameter("@CustomerId", customer.Id) });

                foreach (var item in plans)
                {
                    _dbContext.Database.ExecuteSqlRaw(@"INSERT INTO [OperationPlans] ([Id],
[ServiceId], 
[DeviceId],  
[M1],        
[M2],        
[M3],        
[M4],        
[M5],        
[M6],        
[M7],        
[M8],        
[M9],        
[M10],       
[M11],       
[M12],       
[CustomerId]) VALUES (@Id,
@ServiceId, 
@DeviceId,  
@M1,        
@M2,        
@M3,        
@M4,        
@M5,        
@M6,        
@M7,        
@M8,        
@M9,        
@M10,       
@M11,       
@M12,       
@CustomerId);", new SqlParameter[] {
new SqlParameter("@Id",Guid.NewGuid()),
new SqlParameter("@M1",item.Item1),
new SqlParameter("@M2",item.Item2),
new SqlParameter("@M3",item.Item3),
new SqlParameter("@M4",item.Item4),
new SqlParameter("@M5",item.Item5),
new SqlParameter("@M6",item.Item6),
new SqlParameter("@M7",item.Item7),
new SqlParameter("@M8",item.Item8),
new SqlParameter("@M9",item.Item9),
new SqlParameter("@M10",item.Item10),
new SqlParameter("@M11",item.Item11),
new SqlParameter("@M12",item.Item12),
new SqlParameter("@ServiceId",item.Item13),
new SqlParameter("@DeviceId",item.Item14),
new SqlParameter("@CustomerId",customer.Id),
                    });
                }

                await _dbContext.Database.CommitTransactionAsync();

                return customer;
            }
            return BadRequest(customer);
        }

        class PlanDef
        {
            public Guid ServiceId { get; set; }
            public Guid DeviceId { get; set; }

            public bool M1 { get; set; }
            public bool M2 { get; set; }
            public bool M3 { get; set; }
            public bool M4 { get; set; }
            public bool M5 { get; set; }
            public bool M6 { get; set; }
            public bool M7 { get; set; }
            public bool M8 { get; set; }
            public bool M9 { get; set; }
            public bool M10 { get; set; }
            public bool M11 { get; set; }
            public bool M12 { get; set; }

        }

        [HttpDelete("DeleteCustomer")]
        public async Task<IActionResult> DeleteCustomer(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).CanDelete) return Unauthorized();

            var cust = await _dbContext.Customers.Where(x => x.Id == id).FirstOrDefaultAsync();
            if (cust == null) return NotFound();

            _dbContext.Customers.Remove(cust);
            _dbContext.Database.ExecuteSqlRaw("DELETE FROM [OperationPlans] WHERE [CustomerId]=@CustomerId;", new SqlParameter[] { new SqlParameter("@CustomerId", id) });
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        // -- Users --

        [HttpGet("GetAllUsers")]
        public async Task<ActionResult<IEnumerable<User>>> GetAllUsers([FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            return await _dbContext.Users.ToArrayAsync();
        }

        [HttpPut("MakeUserAdmin")]
        public async Task<IActionResult> MakeUserAdmin(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.IsAdmin = true;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("AllowUserToEdit")]
        public async Task<IActionResult> AllowUserToEdit(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.CanEdit = true;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("AllowUserToDelete")]
        public async Task<IActionResult> AllowUserToDelete(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.CanDelete = true;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("RemoveUserAdmin")]
        public async Task<IActionResult> RemoveUserAdmin(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            var userIdX = sessionService.GetUser(secToken).Id;
            if (user.Id == userIdX) return BadRequest("Can not edit itself.");

            user.IsAdmin = false;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("DisallowUserToEdit")]
        public async Task<IActionResult> DisallowUserToEdit(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.CanEdit = false;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("DisllowUserToDelete")]
        public async Task<IActionResult> DisllowUserToDelete(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.CanDelete = false;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("ActivateUser")]
        public async Task<IActionResult> ActivateUser(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.IsActive = true;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpPut("DeactivateUser")]
        public async Task<IActionResult> DeactivateUser(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();

            var user = await _dbContext.Users.FirstOrDefaultAsync(x => x.Id == userId);
            if (user == null) return NotFound();

            user.IsActive = false;
            await _dbContext.SaveChangesAsync();
            return Ok();
        }

        [HttpDelete("DeleteUser")]
        public async Task<IActionResult> DeleteUser(Guid userId, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (!sessionService.GetUser(secToken).IsAdmin) return Unauthorized();


            var user = await _dbContext.Users.Where(x => x.Id == userId).FirstOrDefaultAsync();
            if (user == null) return NotFound();
            if (user.IsAdmin) return BadRequest("Can not delete admins.");

            var userIdX = sessionService.GetUser(secToken).Id; //User.Claims.Where(x => x.Type == ClaimTypes.NameIdentifier).Select(x => x.Value).FirstOrDefault();
            if (user.Id == userIdX) return BadRequest("Can not delete itself.");

            _dbContext.Users.Remove(user);

            await _dbContext.SaveChangesAsync();
            return Ok();
        }


        /*
        [HttpGet("GetRepresentativesCount")]
        [HttpGet("GetAllRepresentatives")]
        [HttpGet("GetRepresentatives")]
        [HttpPost("CreateRepresentative")]
        [HttpPut("EditRepresentative")]
        [HttpDelete("DeleteRepresentative")]
        */


        // -- Reporting APIs

        [HttpGet("GetCustomerReportData")]
        public async Task<ActionResult<CustomerReportingModel>> GetCustomerReportData(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            var cust = await _dbContext.Customers.Include(x => x.Representative).Include(x => x.Plans).ThenInclude(x => x.Service).Include(x => x.Plans).ThenInclude(x => x.Device).Where(x => x.Id == id).Select(x => new CustomerReportingModel
            {
                Email = x.Email,
                IsCustomer = x.IsCustomer,
                NameAr = x.NameAr,
                NameEn = x.NameEn,
                Notes = x.Notes,
                RepresentativeName = x.Representative.Name,
                ResponsibleName = x.ResponsibleName,
                StartingDate = x.StartingDate,
                Telephone = x.Telephone,
                Plans = x.Plans.Select(z => new PlanReportingModel
                {
                    DeviceName = z.Device.NameAr,
                    M1 = z.M1,
                    M10 = z.M10,
                    M11 = z.M11,
                    M12 = z.M12,
                    M2 = z.M2,
                    M3 = z.M3,
                    M4 = z.M4,
                    M5 = z.M5,
                    M6 = z.M6,
                    M7 = z.M7,
                    M8 = z.M8,
                    M9 = z.M9,
                    ServiceName = z.Service.NameAr
                }).ToList()
            }).FirstOrDefaultAsync();

            return cust;
        }

        [HttpGet("GetSupervisorReportData")]
        public async Task<ActionResult<SupervisorReportingModel>> GetSupervisorReportData(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Supervisors.Where(x => x.Id == id).Select(x => new SupervisorReportingModel
            {
                Address = x.Address,
                BirthDate = x.BirthDate,
                Telephone = x.Telephone,
                LicenseNumber = x.LicenseNumber,
                Id = x.Id,
                IsActive = x.IsActive,
                LicenseDate = x.LicenseDate,
                LicenseEndDate = x.LicenseEndDate,
                Name = x.Name,
                Nationality = x.Nationality,
                PassportDate = x.PassportDate,
                PassportEndDate = x.PassportEndDate,
                PassportNumber = x.PassportNumber,
                ResidencyDate = x.ResidencyDate,
                ResidencyEndDate = x.ResidencyEndDate,
                ResidencyNumber = x.ResidencyNumber,
                WorkStartingDate = x.WorkStartingDate
            }).FirstOrDefaultAsync();
        }

        [HttpGet("GetRepresentativeReportData")]
        public async Task<ActionResult<RepresentativeReportingModel>> GetRepresentativeReportData(Guid id, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            return await _dbContext.Representatives.Where(x => x.Id == id).Select(x => new RepresentativeReportingModel
            {
                Address = x.Address,
                BirthDate = x.BirthDate,
                Telephone = x.Telephone,
                LicenseNumber = x.LicenseNumber,
                Id = x.Id,
                IsActive = x.IsActive,
                LicenseDate = x.LicenseDate,
                LicenseEndDate = x.LicenseEndDate,
                Name = x.Name,
                Nationality = x.Nationality,
                PassportDate = x.PassportDate,
                PassportEndDate = x.PassportEndDate,
                PassportNumber = x.PassportNumber,
                ResidencyDate = x.ResidencyDate,
                ResidencyEndDate = x.ResidencyEndDate,
                ResidencyNumber = x.ResidencyNumber,
                WorkStartingDate = x.WorkStartingDate
            }).FirstOrDefaultAsync();
        }

        [HttpPost("GetJobOrderReportData")]
        public async Task<ActionResult<JobOrderReportingModel>> GetJobOrderReportData([FromBody] JobOrderEditModel editModel, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            IQueryable<JobOrder> jobOrders = _dbContext.JobOrders.Include(x => x.Customer).Include(x => x.Representative).Include(x => x.Supervisor);
            // Filter JobOrders: هنا ممكن تكمل باقى الفلتر
            if (!String.IsNullOrWhiteSpace(editModel.InvoiceNumber) && editModel.InvoiceNumber!="0")
                jobOrders = jobOrders.Where(x => x.InvoiceNumber.ToLower() == editModel.InvoiceNumber.ToLower());

            if (editModel.CustomerId != Guid.Empty)
                jobOrders = jobOrders.Where(x => x.Customer.Id == editModel.CustomerId);

            if (!String.IsNullOrWhiteSpace(editModel.ResponsibleName))
                jobOrders = jobOrders.Where(x => x.ResponsibleName.ToLower() == editModel.ResponsibleName.ToLower());

            if (!String.IsNullOrWhiteSpace(editModel.ResponsibleTelephone))
                jobOrders = jobOrders.Where(x => x.ResponsibleTelephone.ToLower() == editModel.ResponsibleTelephone.ToLower());



            return await jobOrders.Select(x => new JobOrderReportingModel
            {
                ApproveSparePartsChange = x.ApproveSparePartsChange,
                CustomerName = x.Customer.NameAr,
                Done = x.Done,
                InvoiceNumber = x.InvoiceNumber,
                IsDone = x.IsDone,
                MaintenanceAndRepair = x.MaintenanceAndRepair,
                MaintenanceOfExtinguishers = x.MaintenanceOfExtinguishers,
                RepairFailures = x.RepairFailures,
                ReportPreparing = x.ReportPreparing,
                RepresentativeName = x.Representative.Name,
                ResponsibleName = x.ResponsibleName,
                ResponsibleTelephone = x.ResponsibleTelephone,
                SiteStudy = x.SiteStudy,
                SpareParts = x.SpareParts,
                Statement = x.Statement,
                SupervisorName = x.Supervisor.Name
            }).FirstOrDefaultAsync();
        }

        // report strings

        //[HttpGet("GetCustomerReportString")]
        //public async Task<ActionResult<string>> GetCustomerReportString(Guid id, [FromQuery] Guid secToken) { return ""; }

        //[HttpGet("GetSupervisorReportString")]
        //public async Task<ActionResult<string>> GetSupervisorReportString(Guid id, [FromQuery] Guid secToken) { return ""; }

        //[HttpGet("GetRepresentativeReportString")]
        //public async Task<ActionResult<string>> GetRepresentativeReportString(Guid id, [FromQuery] Guid secToken) { return ""; }

        //[HttpGet("GetJobOrderReportString")]
        //public async Task<ActionResult<string>> GetJobOrderReportString(Guid id, [FromQuery] Guid secToken) { return ""; }

        //-- login

        [HttpPost("Login")]
        public async Task<ActionResult<LoginResult>> Login(LoginInput loginInput)
        {
            var user = await _dbContext.Users.Where(x => x.UserName.ToLower() == loginInput.UserName.ToLower() && x.Password == HashPassword(loginInput.Password)).FirstOrDefaultAsync();

            if (user == null) return BadRequest();

            var token = sessionService.LogIn(user);
            if (user.IsActive)
                return new LoginResult { UserId = user.Id, UserName = user.UserName, IsAdmin = user.IsAdmin, CanEdit = user.IsAdmin || user.CanEdit, CanDelete = user.IsAdmin || user.CanDelete, SecToken = token, IsLoggedIn = true, IsActive = user.IsActive };
            else return BadRequest("Invalid login information or not active user.");
        }

        [HttpGet("LogOut")]
        public async Task<IActionResult> LogOut(Guid secToken)
        {
            sessionService.LogOut(secToken);
            return Ok();
        }

        [HttpPost("Register")]
        public async Task<ActionResult<LoginResult>> Register(LoginInput loginInput)
        {
            var user = await _dbContext.Users.Where(x => x.UserName.ToLower() == loginInput.UserName.ToLower()).FirstOrDefaultAsync();

            if (user != null) return BadRequest("User with the same UserName already exists.");

            _dbContext.Users.Add(new Models.User { UserName = loginInput.UserName, Password = HashPassword(loginInput.Password) });

            await _dbContext.SaveChangesAsync();

            return Ok();
        }

        [HttpPost("ChangePassword")]
        public async Task<ActionResult<LoginResult>> ChangePassword(ChangePasswordModel changePasswordModel, [FromQuery] Guid secToken)
        {
            if (!sessionService.LoggedIn(secToken)) return Unauthorized();

            if (changePasswordModel.NewPassword.Trim().Length < 4) return BadRequest("Password should be more than 3 character.");

            var user = await _dbContext.Users.Where(x => x.Id == sessionService.GetUser(secToken).Id).FirstOrDefaultAsync();

            //if (user == null) return NotFound();

            if (user.Password != HashPassword(changePasswordModel.OldPassword)) return BadRequest("Old password not valid.");

            user.Password = HashPassword(changePasswordModel.NewPassword.Trim());

            await _dbContext.SaveChangesAsync();

            await LogOut(secToken);

            return Ok();
        }

        [NonAction]
        string HashPassword(string password)
        {
            var sha1 = new System.Security.Cryptography.SHA1CryptoServiceProvider();
            var sha1data = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            return Convert.ToBase64String(sha1data);
        }
    }

    public class LoadDataArgs
    {
        public int Skip { get; set; }

        public int Top { get; set; }

        public string OrderBy { get; set; }

        public string Filter { get; set; }
    }

    public class NamedItem
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }

    public class CustomerInputModel
    {
        public string NameAr { get; set; }

        public string NameEn { get; set; }

        public Guid RepresentativeId { get; set; }

        public string ResponsibleName { get; set; }

        public bool IsCustomer { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public DateTimeOffset? StartingDate { get; set; }

        public string Notes { get; set; }

        public ICollection<OperationPlan> Plans { get; set; }

    }

    public class CustomerGridViewModel
    {
        public Guid Id { get; set; }
        public string NameAr { get; set; }

        public string NameEn { get; set; }

        public string RepresentativeName { get; set; }

        public string ResponsibleName { get; set; }

        public bool IsCustomer { get; set; }

    }

    public class JobOrderRowModel
    {
        public Guid Id { get; set; }

        public string InvoiceNumber { get; set; }

        public string CustomerName { get; set; }

        public string RepresentativeName { get; set; }

        public string SupervisorName { get; set; }

        public string ResponsibleName { get; set; }

        public string ResponsibleTelephone { get; set; }

        public bool IsDone { get; set; }
    }

    public class JobOrderEditModel
    {
        public Guid Id { get; set; }

        public string InvoiceNumber { get; set; }

        public Guid CustomerId { get; set; }

        public Guid RepresentativeId { get; set; }

        public Guid SupervisorId { get; set; }

        public string ResponsibleName { get; set; }

        public string ResponsibleTelephone { get; set; }

        public string SpareParts { get; set; }

        public string Statement { get; set; }

        public string Done { get; set; }

        public bool ReportPreparing { get; set; }

        public bool MaintenanceOfExtinguishers { get; set; }

        public bool MaintenanceAndRepair { get; set; }

        public bool RepairFailures { get; set; }

        public bool ApproveSparePartsChange { get; set; }

        public bool SiteStudy { get; set; }

        public bool IsDone { get; set; }

        public int BatchNumber { get; set; }
    }

    public class CustomerRowModel
    {
        public Guid Id { get; set; }

        public string NameAr { get; set; }

        public string NameEn { get; set; }

        public string RepresentativeName { get; set; }

        public string ResponsibleName { get; set; }

        public bool IsCustomer { get; set; }

    }

    // --- Reporting models ---

    public class PlanReportingModel
    {
        public string ServiceName { get; set; }
        public string DeviceName { get; set; }
        public bool M1 { get; set; }
        public bool M2 { get; set; }
        public bool M3 { get; set; }
        public bool M4 { get; set; }
        public bool M5 { get; set; }
        public bool M6 { get; set; }
        public bool M7 { get; set; }
        public bool M8 { get; set; }
        public bool M9 { get; set; }
        public bool M10 { get; set; }
        public bool M11 { get; set; }
        public bool M12 { get; set; }
    }

    public class CustomerReportingModel
    {
        public string NameAr { get; set; }
        public string NameEn { get; set; }
        public string RepresentativeName { get; set; }
        public string ResponsibleName { get; set; }
        public bool IsCustomer { get; set; }
        public string Email { get; set; }
        public string Telephone { get; set; }
        public DateTimeOffset? StartingDate { get; set; }
        public string Notes { get; set; }

        public List<PlanReportingModel> Plans { get; set; } = new List<PlanReportingModel>();

    }

    public class SupervisorReportingModel : Supervisor
    {

    }

    public class RepresentativeReportingModel : Representative
    {

    }

    public class JobOrderReportingModel
    {
        public string InvoiceNumber { get; set; }

        public string CustomerName { get; set; }

        public string RepresentativeName { get; set; }

        public string SupervisorName { get; set; }

        public string ResponsibleName { get; set; }

        public string ResponsibleTelephone { get; set; }

        public string SpareParts { get; set; }

        public string Statement { get; set; }

        public string Done { get; set; }

        public bool ReportPreparing { get; set; }

        public bool MaintenanceOfExtinguishers { get; set; }

        public bool MaintenanceAndRepair { get; set; }

        public bool RepairFailures { get; set; }

        public bool ApproveSparePartsChange { get; set; }

        public bool SiteStudy { get; set; }

        public bool IsDone { get; set; }

        public int BatchNumber { get; set; }
    }

    public class LoginInput
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }

    public class ChangePasswordModel
    {
        public string OldPassword { get; set; }
        public string NewPassword { get; set; }
    }

    public class LoginResult
    {
        public Guid UserId { get; set; }
        public string UserName { get; set; }
        public bool IsLoggedIn { get; set; }
        public Guid SecToken { get; set; }
        public bool IsAdmin { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool IsActive { get; set; }
    }
}