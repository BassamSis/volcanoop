﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Volcano.Server.Services
{
    public class SessionService
    {
        Dictionary<Guid, Session> Sessions = new();


        public Guid LogIn(Models.User user)
        {
            Guid token = Guid.NewGuid();

            Sessions.Add(token, new Session { User = user, Expire = DateTime.Now.AddHours(2) });

            return token;
        }

        public void LogOut(Guid token)
        {
            if (Sessions.ContainsKey(token))
                Sessions.Remove(token);
        }

        public void LogOutUser(Guid userId)
        {
            Guid token = Guid.Empty;
            foreach (var item in Sessions)
            {
                if (item.Value.User.Id == userId)
                {
                    token = item.Key;
                    break;
                }
            }

            if (token != Guid.Empty) LogOut(token);
        }

        public bool LoggedIn(Guid token)
        {
            if (Sessions.ContainsKey(token))
            {
                if (Sessions[token].Expire > DateTime.Now)
                    return true;
                else
                    LogOut(token);

                return false;
            }
            else return false;
        }

        public Models.User GetUser(Guid token)
        {
            return Sessions[token]?.User;
        }

    }

    public class Session
    {

        public Models.User User { get; set; }

        public DateTime Expire { get; set; }


    }
}
