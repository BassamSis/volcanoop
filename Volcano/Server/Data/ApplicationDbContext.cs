﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Volcano.Server.Models;


namespace Volcano.Server.Data
{
    public class ApplicationDbContext : DbContext // ApiAuthorizationDbContext<ApplicationUser>
    {
        public DbSet<Models.Customer> Customers { get; set; }
        public DbSet<Models.Device> Devices { get; set; }
        public DbSet<Models.JobOrder> JobOrders { get; set; }
        public DbSet<Models.OperationPlan> OperationPlans { get; set; }
        public DbSet<Models.Representative> Representatives { get; set; }
        public DbSet<Models.Service> Services { get; set; }
        public DbSet<Models.Supervisor> Supervisors { get; set; }
        public DbSet<Models.User>  Users { get; set; }
        public ApplicationDbContext(
            DbContextOptions options) : base(options)
        {
        }

        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    builder.Entity<JobOrder>().HasOne(e => e.Representative)
        //        .WithOne().OnDelete(DeleteBehavior.NoAction);




        //    base.OnModelCreating(builder);
        //}
    }
}
