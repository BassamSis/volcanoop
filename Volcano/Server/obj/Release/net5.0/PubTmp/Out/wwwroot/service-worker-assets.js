﻿self.assetsManifest = {
  "assets": [
    {
      "hash": "sha256-GvD0oHsMCb4LgNNyuLp8mg7W8IX+\/LTCqFw0KKGEpjY=",
      "url": "css\/app.css"
    },
    {
      "hash": "sha256-YLGeXaapI0\/5IgZopewRJcFXomhRMlYYjugPLSyNjTY=",
      "url": "css\/bootstrap\/bootstrap.min.css"
    },
    {
      "hash": "sha256-xMZ0SaSBYZSHVjFdZTAT\/IjRExRIxSriWcJLcA9nkj0=",
      "url": "css\/bootstrap\/bootstrap.min.css.map"
    },
    {
      "hash": "sha256-+Q44zfEaCMmXduni5Td+IgCbk8sSUQwES2nWs+KKQz0=",
      "url": "css\/open-iconic\/FONT-LICENSE"
    },
    {
      "hash": "sha256-BJ\/G+e+y7bQdrYkS2RBTyNfBHpA9IuGaPmf9htub5MQ=",
      "url": "css\/open-iconic\/font\/css\/open-iconic-bootstrap.min.css"
    },
    {
      "hash": "sha256-OK3poGPgzKI2NzNgP07XMbJa3Dz6USoUh\/chSkSvQpc=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.eot"
    },
    {
      "hash": "sha256-sDUtuZAEzWZyv\/U1xl\/9D3ehyU69JE+FvAcu5HQ+\/a0=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.otf"
    },
    {
      "hash": "sha256-oUpLdS+SoLJFwf4bzA3iKD7TCm66oLkTpAQlVJ2s1wc=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.svg"
    },
    {
      "hash": "sha256-p+RP8CV3vRK1YbIkNzq3rPo1jyETPnR07ULb+HVYL8w=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.ttf"
    },
    {
      "hash": "sha256-cZPqVlRJfSNW0KaQ4+UPOXZ\/v\/QzXlejRDwUNdZIofI=",
      "url": "css\/open-iconic\/font\/fonts\/open-iconic.woff"
    },
    {
      "hash": "sha256-s\/Is6Ey6jfNAEfXUIOyHrXXX+RcA8hzchYnuOIWUMl4=",
      "url": "css\/open-iconic\/ICON-LICENSE"
    },
    {
      "hash": "sha256-9wdNXQFE78LCNHo+Hq2eXMTx+YBf2gjsufVTJc8dAV0=",
      "url": "css\/open-iconic\/README.md"
    },
    {
      "hash": "sha256-Jtxf9L+5ITKRc1gIRl4VbUpGkRNfOBXjYTdhJD4facM=",
      "url": "favicon.ico"
    },
    {
      "hash": "sha256-IpHFcQvs03lh6W54zoeJRG1jW+VnGmwymzxLoXtKX7Y=",
      "url": "icon-512.png"
    },
    {
      "hash": "sha256-isY80sJN6wjX2\/q\/fLqF2\/awYNFjLPoB6s9UOHeCbbQ=",
      "url": "index.html"
    },
    {
      "hash": "sha256-Qi9Ce0oKnkpUzfbsQ9kAXZG2GLh7nu3Z15HyLRFJjKA=",
      "url": "manifest.json"
    },
    {
      "hash": "sha256-mgBW5yCOln\/S9yuIftZrIJHv0ViuficS47iQm2zY\/d8=",
      "url": "_framework\/OpenAPIs\/swagger.json"
    },
    {
      "hash": "sha256-2OdnPsDgJyoSaSlvebGMRd2MxC+4UIqparqVrQZAFDE=",
      "url": "_framework\/dotnet.timezones.blat"
    },
    {
      "hash": "sha256-YXYNlLeMqRPFVpY2KSDhleLkNk35d9KvzzwwKAoiftc=",
      "url": "_framework\/dotnet.wasm"
    },
    {
      "hash": "sha256-m7NyeXyxM+CL04jr9ui1Z6pVfMWwhHusuz5qNZWpAwA=",
      "url": "_framework\/icudt.dat"
    },
    {
      "hash": "sha256-91bygK5voY9lG5wxP0\/uj7uH5xljF9u7iWnSldT1Z\/g=",
      "url": "_framework\/icudt_CJK.dat"
    },
    {
      "hash": "sha256-DPfeOLph83b2rdx40cKxIBcfVZ8abTWAFq+RBQMxGw0=",
      "url": "_framework\/icudt_EFIGS.dat"
    },
    {
      "hash": "sha256-oM7Z6aN9jHmCYqDMCBwFgFAYAGgsH1jLC\/Z6DYeVmmk=",
      "url": "_framework\/icudt_no_CJK.dat"
    },
    {
      "hash": "sha256-cUOztblhuGlkjvgM72ql0e2NRuVqwW32qti8huxzYXs=",
      "url": "_framework\/dotnet.5.0.8.js"
    },
    {
      "hash": "sha256-YQ9fMXpf3w\/NlCWIGPx\/QSqYd6+yOGLo3IcqxVFqvc0=",
      "url": "Volcano.Client.styles.css"
    },
    {
      "hash": "sha256-tVlrrRRH3A46pk8D4kt3SqWk\/EXDeI8X734Tas5mKwg=",
      "url": "_content\/Radzen.Blazor\/css\/dark-base.css"
    },
    {
      "hash": "sha256-cF95yabbpBFRjHhG17oDyA5FypxtPC3MUdwyxtGXEFc=",
      "url": "_content\/Radzen.Blazor\/css\/dark.css"
    },
    {
      "hash": "sha256-cmVEhFVRQ17SOpyhO1O6gHARp0qr9934qzAYJFJl1lg=",
      "url": "_content\/Radzen.Blazor\/css\/default-base.css"
    },
    {
      "hash": "sha256-8k8kYkSWyFvcfMP1suW2r2+kdG+guUu12BZHy3F96iE=",
      "url": "_content\/Radzen.Blazor\/css\/default.css"
    },
    {
      "hash": "sha256-mhwf0lg1zlsMLjRMrcEL9w4cumysXW2S1qoHReRGF2U=",
      "url": "_content\/Radzen.Blazor\/css\/humanistic-base.css"
    },
    {
      "hash": "sha256-yoQ\/R06\/N0KF4q\/CddzerR48w3bCc4yGm3snpXFxoMQ=",
      "url": "_content\/Radzen.Blazor\/css\/humanistic.css"
    },
    {
      "hash": "sha256-pIBp0fr9wnqgmh9+fhYx1kETFZzW4BDXSnmENT2Gr44=",
      "url": "_content\/Radzen.Blazor\/css\/software-base.css"
    },
    {
      "hash": "sha256-fIRBCt2LmtjyPgIKyYEoDeX+Ea4VFvhqGDIC7qackVQ=",
      "url": "_content\/Radzen.Blazor\/css\/software.css"
    },
    {
      "hash": "sha256-RzlsXbNxwUdW9SjnSADcv4ufIePiPQ8VPowGo0BB0mQ=",
      "url": "_content\/Radzen.Blazor\/fonts\/MaterialIcons-Regular.woff"
    },
    {
      "hash": "sha256-aY5euu4b9B4uoPbO6pUg0M02KHH1iFkV\/DX7TpV\/l+g=",
      "url": "_content\/Radzen.Blazor\/fonts\/roboto-v15-latin-300.woff"
    },
    {
      "hash": "sha256-PrZc6Ar6Orw126gGmRpfnzIY2LU8S+T5wSSNnZ88Guo=",
      "url": "_content\/Radzen.Blazor\/fonts\/roboto-v15-latin-700.woff"
    },
    {
      "hash": "sha256-l7uYY0Ka6X\/MDNbIDTDD90VNCyGNR1jiTDC9pEG9OdM=",
      "url": "_content\/Radzen.Blazor\/fonts\/roboto-v15-latin-regular.woff"
    },
    {
      "hash": "sha256-4tJ2y83Opy9zhl\/cqC+2kN6Dyj9o+FR2zLmn9aSPwcg=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-Black.woff"
    },
    {
      "hash": "sha256-Se1Ckp\/WChi38Ylv0M9zeWV40BrNdzJZCuL5vLArVss=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-BlackIt.woff"
    },
    {
      "hash": "sha256-iwPDiY3GwHRvR7nxbjO1MUzZ4OIDrBnBE7JVh8uvL7Q=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-Bold.woff"
    },
    {
      "hash": "sha256-hbpdyuMzEkbBLV46tNBW8O4fcBC1OHCZu73IKA\/wxFY=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-BoldIt.woff"
    },
    {
      "hash": "sha256-PLpVAO2hf3ujhcFgIgpvr95XZj15sDT5TlPjBcIKjOo=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-ExtraLight.woff"
    },
    {
      "hash": "sha256-gyjJCK9yyzeB56egUKX2oyJUAHzhrxPnuT+JB1brUjg=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-ExtraLightIt.woff"
    },
    {
      "hash": "sha256-Yi16KO1MmnQJMcyA7nBl9RenDCtzOoi9TIYcCTDYYA0=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-It.woff"
    },
    {
      "hash": "sha256-U\/c5uZXyrUZYibduYAqP66xUX7hjF+MuGqjty6kp+n8=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-Light.woff"
    },
    {
      "hash": "sha256-Wz06DrCrEo8PhVFosBvtv0BwVRad1e6cHVYPrOXcTTk=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-LightIt.woff"
    },
    {
      "hash": "sha256-FUVkwg6zvTHIIS9plEgvWa39AFMb6VCbD1LTENYjsm4=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-Regular.woff"
    },
    {
      "hash": "sha256-0yvxUfB8+6PXLdQAnlit3rprdUByfZdrdEJe4i5AjqY=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-Semibold.woff"
    },
    {
      "hash": "sha256-f7Kf\/RI21ULz4ITMVN+kUuGhpYP3UWYywC73o3WtAgw=",
      "url": "_content\/Radzen.Blazor\/fonts\/SourceSansPro-SemiboldIt.woff"
    },
    {
      "hash": "sha256-e9qDDX2JkeEUuc1MIA8vUVNKhxB\/y3y6fihYftKg2K0=",
      "url": "_content\/Radzen.Blazor\/Radzen.Blazor.js"
    },
    {
      "hash": "sha256-jKRAbRcU16IXt+rLYbFXyTuIEMFEHo2kGbpSZtg\/+Fk=",
      "url": "_framework\/Blazored.SessionStorage.dll"
    },
    {
      "hash": "sha256-f4WVliuncnjCIsytXGPYVvJeCxPmhefX9Dq482ypcQg=",
      "url": "_framework\/Bunit.Core.dll"
    },
    {
      "hash": "sha256-ezR5JsaKNNrmFCVRbqLPSnbQxhoyYn5O\/FDyESdTCAo=",
      "url": "_framework\/Microsoft.AspNetCore.Authorization.dll"
    },
    {
      "hash": "sha256-tBvgbkzlIQJlmpCCmAybAXvOmUiPHJyUwuTAMxXvA\/8=",
      "url": "_framework\/Microsoft.AspNetCore.Components.dll"
    },
    {
      "hash": "sha256-RP7yOfaRJ57OPsGU4qNA91OCV4vHFwVY6W9+qpf+PUY=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Forms.dll"
    },
    {
      "hash": "sha256-F5tH+807\/0f85LK\/zrLdtM94GcIaLK6A7SjiuXLifQk=",
      "url": "_framework\/Microsoft.AspNetCore.Components.Web.dll"
    },
    {
      "hash": "sha256-Cdm6f9NJgz\/s+7R04VwljE1Bg5W0v3f66eA6zdJULnw=",
      "url": "_framework\/Microsoft.AspNetCore.Components.WebAssembly.dll"
    },
    {
      "hash": "sha256-rDfybVJazbLYOyEoAJruMVj5+5tyt72uTGXCCGhWB+8=",
      "url": "_framework\/Microsoft.AspNetCore.Metadata.dll"
    },
    {
      "hash": "sha256-bZZ69\/58+BnN9G33FpTd2iXAdfgLbBnDUiiKxfaZ7IU=",
      "url": "_framework\/Microsoft.Extensions.Configuration.dll"
    },
    {
      "hash": "sha256-7mxFRRlriJzO95ZBz4cmDSFADogdaoDy8MxzQoNtwaU=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Abstractions.dll"
    },
    {
      "hash": "sha256-cwzKuqTaJjYc5QXknt6NaBt\/MWSt9DYHfmu0I\/CV2mc=",
      "url": "_framework\/Microsoft.Extensions.Configuration.Json.dll"
    },
    {
      "hash": "sha256-VUD5PwE+8XAbH7lFgvWuzWnVtag8AOiUbjI01m16xt8=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.dll"
    },
    {
      "hash": "sha256-ILn8wRJDRwF9qMYMY7Mh0ePk7z6\/pJh0HP3moxeZPGw=",
      "url": "_framework\/Microsoft.Extensions.DependencyInjection.Abstractions.dll"
    },
    {
      "hash": "sha256-TpIA498zSsaZAjS+42XzHSmeWdSuY4\/\/ydfZQGlz1O8=",
      "url": "_framework\/Microsoft.Extensions.Logging.dll"
    },
    {
      "hash": "sha256-zTip7aN1jnVJylkadECDBBRNjTxDInVBlG3mZ0k\/cVQ=",
      "url": "_framework\/Microsoft.Extensions.Logging.Abstractions.dll"
    },
    {
      "hash": "sha256-0ceIi\/iLdiKoxaztvCC\/4thA\/DazgsgrTETQXY30M3c=",
      "url": "_framework\/Microsoft.Extensions.Options.dll"
    },
    {
      "hash": "sha256-32yXMbcCMbfUq7vtlIPput8uO7SZK8E9m3V8EXMScBc=",
      "url": "_framework\/Microsoft.Extensions.Primitives.dll"
    },
    {
      "hash": "sha256-wZE69NpcCH5CGYoRgi5KGdQAVhUtfAypcSlnm91TVKg=",
      "url": "_framework\/Microsoft.JSInterop.dll"
    },
    {
      "hash": "sha256-Brid7h+Mh4VCgeNTW5Dzyk2idnAHlgItGP32MlMD1XQ=",
      "url": "_framework\/Microsoft.JSInterop.WebAssembly.dll"
    },
    {
      "hash": "sha256-QLKq03aCPyIA2pKHgdce+PGcC633U2fWjjNNEtZS+js=",
      "url": "_framework\/Newtonsoft.Json.dll"
    },
    {
      "hash": "sha256-Iaa\/bcXw4u25bRUH4k0IIa1FeNejRW9n8sXgXv89e3A=",
      "url": "_framework\/Radzen.Blazor.dll"
    },
    {
      "hash": "sha256-qZYTUS+ga7CydtH54fYPbehFCvx3B49I74G8dxreTog=",
      "url": "_framework\/System.IO.Pipelines.dll"
    },
    {
      "hash": "sha256-Ss9Rwnyn1fJyYDmmxkZu7YHBoHJi7MLx\/o8TiCGeLrc=",
      "url": "_framework\/System.Linq.Dynamic.Core.dll"
    },
    {
      "hash": "sha256-i4\/BXR9cGC3lsBm+\/OAfOy\/8BS7yqAlNOG\/6lRRbT5U=",
      "url": "_framework\/System.Security.Cryptography.Pkcs.dll"
    },
    {
      "hash": "sha256-stfEepmGxj77NWkuqclm0Cvk5x5g0MpNVCLXt5yUbDo=",
      "url": "_framework\/System.Security.Cryptography.Xml.dll"
    },
    {
      "hash": "sha256-TbmvkKV1XqKAjZVVi1OCUdUE2xJPyyWuJ5JMw5eAB1s=",
      "url": "_framework\/System.Security.Permissions.dll"
    },
    {
      "hash": "sha256-qF6oohf0MsTq0kfXw2bJ29juJf3VabESmnoB4twQixs=",
      "url": "_framework\/Volcano.Shared.dll"
    },
    {
      "hash": "sha256-xltLLMcTjTFKSIj8caWIDY9Eda04Rs8fSzX5h+4+6RY=",
      "url": "_framework\/Volcano.Client.dll"
    },
    {
      "hash": "sha256-pBf6jSJr5iT4egptjlt5TvsLdhNDd4\/YmEDpjjnEMoU=",
      "url": "_framework\/Microsoft.CSharp.dll"
    },
    {
      "hash": "sha256-1c2Jvv8nFXT3Tj5sT\/4sgsiHhzRrKiv3+EGyj5wWXik=",
      "url": "_framework\/System.Collections.Concurrent.dll"
    },
    {
      "hash": "sha256-b57TbqZCjdbVbjiMxpzRgt8NbM6iTPqTkhbfhfdxhhY=",
      "url": "_framework\/System.Collections.Immutable.dll"
    },
    {
      "hash": "sha256-9Ik35kFiJ0IFIoRGwHzdWBG2R26v2QNhAJaIM3xgcs0=",
      "url": "_framework\/System.Collections.NonGeneric.dll"
    },
    {
      "hash": "sha256-YTsWfxojtP2vcqSJZ8a9BxPErjnqecqRJ+T7fuli8uM=",
      "url": "_framework\/System.Collections.Specialized.dll"
    },
    {
      "hash": "sha256-Zl\/4N35Y1lyZa+hT7tnNdzOTpTU+\/W4PgZ2\/M3Xu1k4=",
      "url": "_framework\/System.Collections.dll"
    },
    {
      "hash": "sha256-Ow\/669mVNb8jUGQAUd8Icm2JBBUEbN3+tZbGrYKY0hM=",
      "url": "_framework\/System.ComponentModel.Annotations.dll"
    },
    {
      "hash": "sha256-1eAJqA44lc9slPuf8uZPrpTZD7oEaDzsA5fXp2w57PY=",
      "url": "_framework\/System.ComponentModel.Primitives.dll"
    },
    {
      "hash": "sha256-rH0D53dCv\/EsstVtFrBZYenqvPxeRuLJeO8baQkOwGQ=",
      "url": "_framework\/System.ComponentModel.TypeConverter.dll"
    },
    {
      "hash": "sha256-6XpFkkQLuo0xWUyW6e9As2DF4A8loUnjdhzdlBXWIEY=",
      "url": "_framework\/System.ComponentModel.dll"
    },
    {
      "hash": "sha256-dljgVRViTrbvzr++Oyt5KycarhAmvoWSjW+G3aO4sDM=",
      "url": "_framework\/System.Console.dll"
    },
    {
      "hash": "sha256-LeJv85xd\/jBiD3B3nf04asnHhD8x774DeYwzPPlr6e8=",
      "url": "_framework\/System.Data.Common.dll"
    },
    {
      "hash": "sha256-LIEjeOoQRX4q8dMvmf04rDfdNMrMkvaE8wTvEbrS2L0=",
      "url": "_framework\/System.Diagnostics.TraceSource.dll"
    },
    {
      "hash": "sha256-rJxarSBRQ4SkGzpOvCgaOWVLAKSs7Dpi3dMoR+nySus=",
      "url": "_framework\/System.Drawing.Primitives.dll"
    },
    {
      "hash": "sha256-JmM7RXKxwbsElvINDQTCy27TzDxakbL7oZX85dmSIv8=",
      "url": "_framework\/System.IO.FileSystem.dll"
    },
    {
      "hash": "sha256-H3EG6hfPsIWcGxitxrE8WyI+PWzVdtIyhZa+Y7MT\/QE=",
      "url": "_framework\/System.Linq.Expressions.dll"
    },
    {
      "hash": "sha256-D4cWx39oopoGDXmXpG3dHuHWUnCxqHf+5aMyVJ7MO50=",
      "url": "_framework\/System.Linq.Queryable.dll"
    },
    {
      "hash": "sha256-F1hUEuSZLVndj1IXOCrHzwLTRHF8JSlQuV3WtFRv9zo=",
      "url": "_framework\/System.Linq.dll"
    },
    {
      "hash": "sha256-VlGvvZMQdnKqXPgS6vYQuNPTkJvaPA6IsnaJLGKRToc=",
      "url": "_framework\/System.Memory.dll"
    },
    {
      "hash": "sha256-ZebBXJ6PUmHblYrXKJeZQu2VyYeuYZfXuRcKiUJlduc=",
      "url": "_framework\/System.Net.Http.dll"
    },
    {
      "hash": "sha256-e2UFP+tOJS1ENUlMurkb9SxNE8u\/NwKJFKKEltF89GI=",
      "url": "_framework\/System.Net.Primitives.dll"
    },
    {
      "hash": "sha256-KeQVHr7uzt64zvAXO9KbL0NQonCI1brsAhuV63M9c2U=",
      "url": "_framework\/System.Net.Requests.dll"
    },
    {
      "hash": "sha256-KhPSWVbxySZxPKv2rPaAiDBdNgXJGKNaopAccjMjqjE=",
      "url": "_framework\/System.ObjectModel.dll"
    },
    {
      "hash": "sha256-t+HI8M9S8MAMyZYboGqq32S9b5cUgAUn9ENVfptPxZ8=",
      "url": "_framework\/System.Private.Runtime.InteropServices.JavaScript.dll"
    },
    {
      "hash": "sha256-1qPNK4h3hUm3RJNIqwhpGXJjGXNfb9cvicU9C2bgfQ8=",
      "url": "_framework\/System.Private.Uri.dll"
    },
    {
      "hash": "sha256-dLMHvwzohZyJKtNM2W85N1RJQq9wXOxX0vgJUHBDzAA=",
      "url": "_framework\/System.Private.Xml.Linq.dll"
    },
    {
      "hash": "sha256-Ei+vxcpVZZuhYxq2WJwubMpubkuZwcupBgWP0sxwGzY=",
      "url": "_framework\/System.Private.Xml.dll"
    },
    {
      "hash": "sha256-dw+MW5vy+3W8yDus4V0nrm3ESzDO3yCq0T\/j+ANKcqI=",
      "url": "_framework\/System.Runtime.CompilerServices.Unsafe.dll"
    },
    {
      "hash": "sha256-7oYDPzYkszkGsej0JsrKWpMArRZ2A8DNE3TUqUPT578=",
      "url": "_framework\/System.Runtime.InteropServices.RuntimeInformation.dll"
    },
    {
      "hash": "sha256-A2RHFOboA2d+vAFz1mA5XOsrUbcHQ\/iyn2gHucYyuhk=",
      "url": "_framework\/System.Runtime.Numerics.dll"
    },
    {
      "hash": "sha256-f231PXghxsrDJmT++M2LV8swwIsBLPBf0OQ+fYtGSpM=",
      "url": "_framework\/System.Runtime.Serialization.Formatters.dll"
    },
    {
      "hash": "sha256-scq5e6afdl7aJY0guf61IV0VUstbbNCxqhinV\/ela08=",
      "url": "_framework\/System.Runtime.Serialization.Primitives.dll"
    },
    {
      "hash": "sha256-x5ynmxBJj\/KHZhkfk1Rs+b0OZBpnJF5hijPj5RY8HA0=",
      "url": "_framework\/System.Security.AccessControl.dll"
    },
    {
      "hash": "sha256-nija63GDDmL1oSU8DQcrn7Ql1fkPWM7oks511JRx0O0=",
      "url": "_framework\/System.Security.Claims.dll"
    },
    {
      "hash": "sha256-z02u5WUDOPHwWAad5x9lPg2tgFhweobKzHoZs12gQf0=",
      "url": "_framework\/System.Security.Cryptography.Algorithms.dll"
    },
    {
      "hash": "sha256-IlLT0XAqQU5udoyKGR+jWoD1JNv5D3IyVhxMQ6BuTo4=",
      "url": "_framework\/System.Security.Cryptography.Csp.dll"
    },
    {
      "hash": "sha256-XxkjHevipBoKqBWY991MA8N80ChuirIrJRHI7pCBLIM=",
      "url": "_framework\/System.Security.Cryptography.Encoding.dll"
    },
    {
      "hash": "sha256-Yp8y1AknJKLvIuOPZr88\/xuqg1PPcMTV9iq6AeSQlx8=",
      "url": "_framework\/System.Security.Cryptography.Primitives.dll"
    },
    {
      "hash": "sha256-pCWzvcxEGD98OQARmzWIRIAye2a1IBeje+Yi7tIH2kM=",
      "url": "_framework\/System.Security.Cryptography.X509Certificates.dll"
    },
    {
      "hash": "sha256-L37NcxsmpN4GWylLkNw1aAQbPdiRBz7LJLWoyk+TCYY=",
      "url": "_framework\/System.Security.Principal.Windows.dll"
    },
    {
      "hash": "sha256-fQUWP1CabKJo5Wfk6VKWqmJrOmc2c95DuwRCWEPIHI8=",
      "url": "_framework\/System.Text.Encodings.Web.dll"
    },
    {
      "hash": "sha256-Y\/XNry3Y6tTb3YC2MTn2Pt4HVxBQs4uEPw2L\/AOqPsw=",
      "url": "_framework\/System.Text.Json.dll"
    },
    {
      "hash": "sha256-gHBDmLX3HdJILv6tvro\/vyqyb1DR1dnygvvkx8EgEjE=",
      "url": "_framework\/System.Text.RegularExpressions.dll"
    },
    {
      "hash": "sha256-IhBFQL21oAiisBz\/kSis3rw82WiGBG\/dtg8mvleBHjE=",
      "url": "_framework\/System.Web.HttpUtility.dll"
    },
    {
      "hash": "sha256-Vgzy3qjmLMubX7C\/ikQmPo0djxRUxCk3CIDRyttd0pk=",
      "url": "_framework\/System.Private.CoreLib.dll"
    },
    {
      "hash": "sha256-B5Kp16WknlOi8BVzLYzsmIiO+2L7NcdKrTHhFaWBXiw=",
      "url": "_framework\/blazor.boot.json"
    },
    {
      "hash": "sha256-dl8FVRvWOJfOtzIC\/x66QNnBNsT9cAOMrn22GB8fJ8U=",
      "url": "_framework\/blazor.webassembly.js"
    }
  ],
  "version": "vLkvHkzi"
};
