﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Volcano.Server.Models
{
    public class Customer
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string NameAr { get; set; }

        [Required]
        public string NameEn { get; set; }

        //[Required]
        //public Guid RepresentativeId { get; set; }

        //[ForeignKey("RepresentativeId")]
        public Representative Representative { get; set; } //= new Representative();

        public string ResponsibleName { get; set; }

        public bool IsCustomer { get; set; }

        public string Email { get; set; }

        public string Telephone { get; set; }

        public DateTimeOffset? StartingDate { get; set; }

        public string Notes { get; set; }

        public List<OperationPlan> Plans { get; set; } = new List<OperationPlan>();

    }
    public class Device
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string NameAr { get; set; }

        [Required]
        public string NameEn { get; set; }
    }

    public class JobOrder
    {
        [Key]
        public Guid Id { get; set; }

        public string InvoiceNumber { get; set; }

        public Customer Customer { get; set; }

        public Representative Representative { get; set; }

        public Supervisor Supervisor { get; set; }

        public string ResponsibleName { get; set; }

        public string ResponsibleTelephone { get; set; }

        public string SpareParts { get; set; }

        public string Statement { get; set; }

        public string Done { get; set; }

        public bool ReportPreparing { get; set; }

        public bool MaintenanceOfExtinguishers { get; set; }

        public bool MaintenanceAndRepair { get; set; }

        public bool RepairFailures { get; set; }

        public bool ApproveSparePartsChange { get; set; }

        public bool SiteStudy { get; set; }

        public bool IsDone { get; set; }

        public int BatchNumber { get; set; }
    }

    public class OperationPlan
    {
        [Key]
        public Guid Id { get; set; }

        public Service Service { get; set; }

        public Device Device { get; set; }

        public bool M1 { get; set; }

        public bool M2 { get; set; }

        public bool M3 { get; set; }

        public bool M4 { get; set; }

        public bool M5 { get; set; }

        public bool M6 { get; set; }

        public bool M7 { get; set; }

        public bool M8 { get; set; }

        public bool M9 { get; set; }

        public bool M10 { get; set; }

        public bool M11 { get; set; }

        public bool M12 { get; set; }
    }

    public class Representative
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Nationality { get; set; }

        public string Telephone { get; set; }

        public string Address { get; set; }

        public bool IsActive { get; set; }

        public string ResidencyNumber { get; set; }

        public DateTimeOffset? ResidencyDate { get; set; }

        public DateTimeOffset? ResidencyEndDate { get; set; }

        public string PassportNumber { get; set; }

        public DateTimeOffset? PassportDate { get; set; }

        public DateTimeOffset? PassportEndDate { get; set; }

        public string LicenseNumber { get; set; }

        public DateTimeOffset? LicenseDate { get; set; }

        public DateTimeOffset? LicenseEndDate { get; set; }

        public DateTimeOffset? BirthDate { get; set; }

        public DateTimeOffset? WorkStartingDate { get; set; }
    }

    public class Service
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string NameAr { get; set; }

        [Required]
        public string NameEn { get; set; }
    }

    public class Supervisor
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string Nationality { get; set; }

        public string Telephone { get; set; }

        public string Address { get; set; }

        public bool IsActive { get; set; }

        public string ResidencyNumber { get; set; }

        public DateTimeOffset? ResidencyDate { get; set; }

        public DateTimeOffset? ResidencyEndDate { get; set; }

        public string PassportNumber { get; set; }

        public DateTimeOffset? PassportDate { get; set; }

        public DateTimeOffset? PassportEndDate { get; set; }

        public string LicenseNumber { get; set; }

        public DateTimeOffset? LicenseDate { get; set; }

        public DateTimeOffset? LicenseEndDate { get; set; }

        public DateTimeOffset? BirthDate { get; set; }

        public DateTimeOffset? WorkStartingDate { get; set; }
    }

    public class User
    {
        [Key]
        public Guid Id { get; set; }

        [Required]
        public string UserName { get; set; }

        [Required]
        public string Password { get; set; }
        public bool IsAdmin { get; set; }
        public bool CanEdit { get; set; }
        public bool CanDelete { get; set; }
        public bool IsActive { get; set; }
    }
}


